using System;

namespace CSharp
{
    public class Dog
        : Animal
    {
        public Dog(string name)
            : base(name, "bark")
        {
        }


        public void Walk()
        {
            Console.WriteLine($"{Name} is walking...");
        }


        public override void Move()
        {
            Walk();
        }


        public override void Visit(IAnimalBehaviour animalBehaviour)
        {
            animalBehaviour.Execute(this);
        }
    }
}