﻿namespace CSharp
{
    public class SoundVisitor
        : IAnimalBehaviour
    {
        public void Execute(Dog dog)
        {
            dog.MakeSound();
        }


        public void Execute(Fish fish)
        {
            fish.MakeSound();
        }


        public void Execute(Bird bird)
        {
            bird.MakeSound();
        }


        public void Execute(Alien alien)
        {
            alien.MakeSound();
        }


        public void Execute(Cat cat)
        {
            cat.MakeSound();
        }
    }
}