﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CSharp.Disposable;
using CSharp.Namespace1;
using CSharp.Namespace2;

namespace CSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            //PersonStuff();

            //AnimalStuff();

            //InterfaceStuff();

            //GenericFunStuff();

            var c = new C();
            var b = new B(c);
            var a = new A(b);
            var graph = new Graph(a);
            //graph.usage();

            c.Dispose();

            //using (var graph2 = new Graph())
            //{
            //    //graph2.usage();
            //}

            var stream = File.Open("", FileMode.Open);
            stream.Dispose();

            Console.ReadKey();
        }


        private static void GenericFunStuff()
        {
            var nfInstance1 = new NongenericFun();
            var nfInstance2 = new NongenericFun();

            nfInstance1.Makefun();
            nfInstance2.Makefun();

            Console.WriteLine();
            Console.WriteLine();

            var gfInstance1 = new GenericFun<int>();
            var gfInstance2 = new GenericFun<int>();
            var gfInstance3 = new GenericFun<double>();

            gfInstance1.Makefun();
            gfInstance2.Makefun();
            gfInstance3.Makefun();
        }


        private static void InterfaceStuff()
        {
            var something = new Something();
            something.DoSomething();

            ISomething1 something1 = something;
            something1.DoSomething();

            ISomething2 something2 = something;
            something2.DoSomething();
        }


        private static void AnimalStuff()
        {
            var lassie = new Dog("Lassie");
            var chester = new Fish("Chester");
            var tweety = new Bird("Tweety");
            var et = new Alien("E.T.");
            var katie = new Cat("Katie");

            var list = new List<Animal> { lassie, chester, tweety, et, katie };
            var behaviours = new List<IAnimalBehaviour> { new MovementVisitor(), new SoundVisitor(), new MovingSoundAnimalBehaviour() };

            //foreach (var animal in list)
            //{
            //    animal.MakeSound();
            //}

            //foreach (var animal in list)
            //{
            //    animal.Move();
            //}

            foreach (var animalBehaviour in behaviours)
            {
                foreach (var animal in list)
                {
                    animal.Visit(animalBehaviour);
                }
            }
        }


        private static void PersonStuff()
        {
            var ps = new PersonStruct();
            ps.Name = "Dwight";
            ps.Age = 35;

            ps.PrintMe();

            ChangeAgeStruct(ref ps);
            ps.PrintMe();

            ChangeAgeStructOut(out ps);
            ps.PrintMe();


            var p = new Person("Dwight", 35);

            p.PrintMe();

            ChangeAge(p);
            p.PrintMe();
        }


        public static void ChangeAgeStruct(ref PersonStruct ps)
        {
            ps.Age = 25;
        }


        public static void ChangeAgeStructOut(out PersonStruct ps)
        {
            ps = new PersonStruct();
            ps.Name = "Fiona";
            ps.Age = 9;
        }


        public static void ChangeAge(Person p)
        {
            if (p == null)
            {
                return;
            }

            //p.Age = 25;
            try
            {
                p.Age = -1;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"ERROR: {ex.Message}");
            }
        }
    }


    public struct PersonStruct
    {
        public string Name;
        public int Age;


        public void PrintMe()
        {
            Console.WriteLine($"Peron is called {Name} and age is {Age} - {DateTime.Now:HH:mm:ss}");
        }
    }


    public class InvalidAgeException
        : Exception
    {
        public int Age { get; }



        public InvalidAgeException(int age)
            : base("An invalid age was provided")
        {
            Age = age;
        }
    }
}