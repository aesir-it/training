using System;


namespace CSharp
{
    public class Fish
        : Animal
    {
        public Fish(string name)
            : base(name, "blub blub")
        {
        }


        public void Swim()
        {
            Console.WriteLine($"{Name} is swimming");
        }


        public override void Move()
        {
            Swim();
        }


        public override void Visit(IAnimalBehaviour animalBehaviour)
        {
            animalBehaviour.Execute(this);
        }
    }
}