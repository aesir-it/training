﻿using System;

namespace CSharp.Disposable
{
    public class Graph
        //: IDisposable
    {
        private readonly A _a;


        public Graph(A a)
        {
            _a = a;
            //_a = new A();
        }


        //public void Dispose()
        //{
        //    _a?.Dispose();
        //}
    }

    public class A
        //: IDisposable
    {
        private readonly B _b;


        public A(B b)
        {
            _b = b;
            //_b = new B();
        }


        //public void Dispose()
        //{
        //    _b?.Dispose();
        //}
    }

    public class B
        //: IDisposable
    {
        private readonly C _c;


        public B(C c)
        {
            _c = c;
            //_c = new C();
        }


        //public void Dispose()
        //{
        //    _c?.Dispose();
        //}
    }

    public class C
        : IDisposable
    {
        private void ReleaseUnmanagedResources()
        {
            // TODO release unmanaged resources here
        }


        public void Dispose()
        {
            ReleaseUnmanagedResources();
            GC.SuppressFinalize(this);
        }


        ~C()
        {
            ReleaseUnmanagedResources();
        }
    }
}