﻿using System;

namespace CSharp
{
    public class NongenericFun
    {
        private readonly SomeHeavyDependency _ourHeavyDependency;



        public static SomeHeavyDependency SomeHeavyDependency { get; private set; }



        public NongenericFun()
        {
            var randomizer = new Random((int) DateTime.Now.Ticks);
            var value = randomizer.Next(1, 666);

            _ourHeavyDependency = new SomeHeavyDependency(value);
        }


        static NongenericFun()
        {
            SomeHeavyDependency = new SomeHeavyDependency(666);
        }


        public void Makefun()
        {
            Console.WriteLine($"Making fun with our shared heavy-D value {SomeHeavyDependency.Value}");
            Console.WriteLine($"Making fun with our own heavy-D value {_ourHeavyDependency.Value}");
        }
    }


    public class SomeHeavyDependency
    {
        public int Value { get; }



        public SomeHeavyDependency(int value)
        {
            Value = value;
        }
    }


    public class GenericFun<T>
    {
        private readonly SomeHeavyDependency _ourHeavyDependency;



        public static SomeHeavyDependency SomeHeavyDependency { get; private set; }



        public GenericFun()
        {
            var randomizer = new Random((int)DateTime.Now.Ticks);
            var value = randomizer.Next(1, 666);

            _ourHeavyDependency = new SomeHeavyDependency(value);
        }


        static GenericFun()
        {
            var randomizer = new Random((int)DateTime.Now.Ticks);
            var value = randomizer.Next(666, 6666);

            SomeHeavyDependency = new SomeHeavyDependency(value);
        }


        public void Makefun()
        {
            Console.WriteLine($"Making fun with our shared heavy-D value {SomeHeavyDependency.Value}");
            Console.WriteLine($"Making fun with our own heavy-D value {_ourHeavyDependency.Value}");
        }
    }
}