﻿namespace CSharp
{
    public class MovementVisitor
        : IAnimalBehaviour
    {
        public void Execute(Dog dog)
        {
            dog.Walk();
        }


        public void Execute(Fish fish)
        {
            fish.Swim();
        }


        public void Execute(Bird bird)
        {
            bird.Fly();
        }


        public void Execute(Alien alien)
        {
            alien.Warp();
        }


        public void Execute(Cat cat)
        {
            cat.SloMoWalk();
        }
    }
}