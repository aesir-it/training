﻿using System;
using CSharp.Namespace1;
using CSharp.Namespace2;


namespace CSharp
{
    public class Something
        : ISomething1, ISomething2
    {
        public void DoSomething()
        {
            Console.WriteLine("ISomething1.DoSomething");
        }


        //void ISomething1.DoSomething()
        //{
        //    Console.WriteLine("ISomething1.DoSomething");
        //}


        void ISomething2.DoSomething()
        {
            Console.WriteLine("ISomething2.DoSomething");
        }
    }
}