using System;


namespace CSharp
{
    public class Bird
        : Animal
    {
        public Bird(string name)
            : base(name, "chirp chirp")
        {
        }


        public void Fly()
        {
            Console.WriteLine($"{Name} is flying...");
        }


        public override void Move()
        {
            Fly();
        }


        public override void Visit(IAnimalBehaviour animalBehaviour)
        {
            animalBehaviour.Execute(this);
        }
    }
}