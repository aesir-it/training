namespace CSharp
{
    public class MovingSoundAnimalBehaviour
        : IAnimalBehaviour
    {
        public void Execute(Dog dog)
        {
            dog.Walk();
            dog.MakeSound();
        }


        public void Execute(Fish fish)
        {
            fish.Swim();
            fish.MakeSound();
        }


        public void Execute(Bird bird)
        {
            bird.Fly();
            bird.MakeSound();
        }


        public void Execute(Alien alien)
        {
            alien.Warp();
            alien.MakeSound();
        }


        public void Execute(Cat cat)
        {
            cat.SloMoWalk();
            cat.MakeSound();
        }
    }
}