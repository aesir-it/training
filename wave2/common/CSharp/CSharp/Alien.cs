﻿using System;

namespace CSharp
{
    public class Alien
        : Animal
    {
        public Alien(string name)
            : base(name, "zorg zorg")
        {
        }


        public void Warp()
        {
            Console.WriteLine($"{Name} is warping...");
        }


        public override void Move()
        {
            Warp();
        }


        public override void Visit(IAnimalBehaviour animalBehaviour)
        {
            animalBehaviour.Execute(this);
        }
    }


    public class Cat
        : Animal
    {
        public Cat(string name)
            : base(name, "miauw")
        {
        }


        public void SloMoWalk()
        {
            Console.WriteLine($"{Name} is slo-mo walking");
        }


        public override void Move()
        {
            SloMoWalk();
        }


        public override void Visit(IAnimalBehaviour animalBehaviour)
        {
            animalBehaviour.Execute(this);
        }
    }
}