using System;


namespace CSharp
{
    public class Person
    {
        private readonly string _name;


        private int _age;


        public int Age
        {
            get { return _age; }
            set { ChangeAge(value); }
        }


        public Person(string name, int age)
        {
            _name = name;
            Age = age;
        }


        private void ChangeAge(int value)
        {
            if (value < 0)
            {
                throw new InvalidAgeException(value);
            }

            _age = value;
        }


        public void PrintMe()
        {
            Console.WriteLine($"Peron is called {_name} and age is {Age} - {DateTime.Now:HH:mm:ss}");
        }
    }
}