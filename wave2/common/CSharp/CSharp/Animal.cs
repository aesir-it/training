using System;


namespace CSharp
{
    public abstract class Animal
    {
        public string Name { get; }


        public string Sound { get; }



        protected Animal(string name, string sound)
        {
            Name = name;
            Sound = sound;
        }


        public void MakeSound()
        {
            Console.WriteLine($"{Name} is making sound {Sound}");
        }


        public abstract void Move();


        public abstract void Visit(IAnimalBehaviour animalBehaviour);
    }

    public interface IAnimalBehaviour
    {
        void Execute(Dog dog);


        void Execute(Fish fish);


        void Execute(Bird bird);


        void Execute(Alien alien);


        void Execute(Cat cat);
    }
}