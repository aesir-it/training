﻿using System;
using NUnit.Framework;


namespace CSharp.Test
{
    [TestFixture]
    public class PersonTest
    {
        [Test]
        public void GivenValidAgeShouldSet()
        {
            // 3A: Arrange Act Assert

            // SYSTEM UNDER TEST

            // Arrange.
            const string name = "Dwight";
            const int age = 25;

            // Act.
            var sut = new Person(name, age);

            // Assert.
            Assert.AreEqual(age, sut.Age);
        }



        [Test]
        public void GivenInvalidAgeShouldThrowInvalidAgeException()
        {
            // 3A: Arrange Act Assert

            // SYSTEM UNDER TEST
            
            // Arrange.
            const string name = "Dwight";
            const int age = -1;
            InvalidAgeException exception = null;

            // Act.
            try
            {
                var sut = new Person(name, age);
            }
            catch (InvalidAgeException ex)
            {
                exception = ex;
            }

            // Assert.
            Assert.IsNotNull(exception);
            Assert.AreEqual(age, exception.Age);
        }
    }
}