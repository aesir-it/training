﻿CREATE TABLE [dbo].[HotAirBalloon] (
    [HotAirBalloonId] INT NOT NULL,
    CONSTRAINT [PK_HotAirBalloon] PRIMARY KEY CLUSTERED ([HotAirBalloonId] ASC),
    CONSTRAINT [FK_HotAirBalloon_FlyingObject] FOREIGN KEY ([HotAirBalloonId]) REFERENCES [dbo].[FlyingObject] ([FlyingObjectId])
);

