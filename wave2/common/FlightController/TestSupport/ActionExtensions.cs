﻿using System;


namespace TestSupport
{
    public static class ActionExtensions
    {
        public static ExceptionCatcher ExecuteWithCatch(this Action action)
        {
            var exceptionCatcher = new ExceptionCatcher();

            exceptionCatcher.Execute(action);

            return exceptionCatcher;
        }
    }
}