﻿using System.Data.Entity;
using FlightController.Infra.EF.Model;


namespace FlightController.Infra.EF.UnitOfWork
{
    public class FlightControllerContext
        : DbContext
    {
        public DbSet<AirportPersistanceModel> Airports { get; set; }



        public FlightControllerContext()
            : base("FlightController")
        {
        }
    }
}