﻿using System;
using FlightController.Domain.Model;
using FlightController.Domain.Services.Contracts;


namespace FlightController.Domain.Services
{
    public class DifferentAirportFlightRouteValidationService
        : IFlightRouteValidationService
    {
        public void Validate(Airport departure, Airport destination)
        {
            var areSame = departure == destination;
            if (areSame)
            {
                throw new Exception();
            }
        }
    }
}