﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightController.Domain.Model;
using FlightController.Domain.Services.Contracts;


namespace FlightController.Domain.Services
{
    public class FlightControllerService
        : IFlightControllerService
    {
        private readonly List<FlyingObject> _flyingObjects;
        private readonly List<FlightRoute> _flightRoutes;



        public FlightControllerService(List<FlyingObject> flyingObjects, List<FlightRoute> flightRoutes)
        {
            _flyingObjects = flyingObjects;
            _flightRoutes = flightRoutes;
        }


        public FlightRunResult Run()
        {
            var start = DateTime.Now;

            var randomizer = new Random(Environment.TickCount);

            var activeFlyingObjects = _flyingObjects.ToList();

            do
            {
                foreach (var flyingObject in activeFlyingObjects.ToList())
                {
                    var potentialFlightRoutes =
                        _flightRoutes
                            .Where(x => x.Departure == flyingObject.CurrentAirport)
                            .ToList();

                    if (potentialFlightRoutes.Count == 0)
                    {
                        activeFlyingObjects.Remove(flyingObject);

                        continue;
                    }

                    var selectedIndex = randomizer.Next(0, potentialFlightRoutes.Count);

                    var flightRoute = potentialFlightRoutes[selectedIndex];

                    flyingObject.TakeOff(flightRoute);
                    flyingObject.Land();
                }
            } while (activeFlyingObjects.Count != 0);

            var stop = DateTime.Now;

            var delta = stop - start;

            return new FlightRunResult(_flyingObjects, delta);
        }
    }
}