﻿using FlightController.Domain.Calculators.Contracts;
using FlightController.Domain.Services.Contracts;


namespace FlightController.Domain.Model
{
    public class FlightRoute
    {
        public Airport Departure { get; }


        public Airport Destination { get; }


        public double Distance { get; }



        private FlightRoute(Airport departure, Airport destination, double distance)
        {
            Departure = departure;
            Destination = destination;
            Distance = distance;
        }



        public static FlightRoute Create(Airport departure, Airport destination, IFlightRouteValidationService flightRouteValidationService, IDistanceCalculator distanceCalculator)
        {
            flightRouteValidationService.Validate(departure, destination);

            var distance = distanceCalculator.Calculate(departure, destination);

            return new FlightRoute(departure, destination, distance);
        }
    }
}