﻿namespace FlightController.Domain.Model
{
    public abstract class FlyingObject
    {
        private FlightRoute _flightRoute;



        public int Id { get; }


        public string Code { get; }


        public Airport CurrentAirport { get; private set; }


        public double AirMiles { get; private set; }


        public int TotalFlights { get; private set; }



        protected FlyingObject(int id, string code, Airport currentAirport)
        {
            Id = id;
            Code = code;
            CurrentAirport = currentAirport;
        }


        public void TakeOff(FlightRoute flightRoute)
        {
            CurrentAirport = Airport.Null;
            TotalFlights++;

            _flightRoute = flightRoute;
        }


        public void Land()
        {
            CurrentAirport = _flightRoute.Destination;
            AirMiles += _flightRoute.Distance;
        }
    }
}