﻿namespace FlightController.Domain.Model
{
    public class Airplane
        : FlyingObject
    {
        public Airplane(int id, string code, Airport currentAirport)
            : base(id, code, currentAirport)
        {
        }
    }
}