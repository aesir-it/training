﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace FlightController.Domain.Model
{
    public class FlightRunResult
    {
        private readonly TimeSpan _delta;
        private readonly List<FlyingObject> _flyingObjects;



        public int TotalFlightCount => _flyingObjects.Sum(x => x.TotalFlights);


        public double TotalDuration => _delta.TotalMilliseconds;



        public FlightRunResult(List<FlyingObject> flyingObjects, TimeSpan delta)
        {
            _delta = delta;
            _flyingObjects = flyingObjects;
        }
    }
}