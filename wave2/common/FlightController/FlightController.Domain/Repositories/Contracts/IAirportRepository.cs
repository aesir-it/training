﻿using FlightController.Domain.Model;


namespace FlightController.Domain.Repositories.Contracts
{
    public interface IAirportRepository
    {
        void Create(Airport airport);


        Airport Get(string code);
    }
}