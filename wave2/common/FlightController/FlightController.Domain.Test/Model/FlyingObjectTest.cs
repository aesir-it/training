﻿using FlightController.Domain.Calculators.Contracts;
using FlightController.Domain.Model;
using FlightController.Domain.Services;
using Moq;
using NUnit.Framework;
using TestSupport;


// ReSharper disable once CheckNamespace
namespace FlightController.Domain.Test.Model.FlyingObjectTest
{
    [TestFixture]
    public class Given_Valid_Dependencies_When_Constructing_Instance_With_Mock_Library
         : ArrangeActAssertTest
    {
        private FlyingObject _sut;
        private int _id;
        private string _code;
        private Airport _currentAirport;



        protected override void Arrange()
        {
            _id = 666;
            _code = "123-ABC";
            _currentAirport = new Airport("BRU");
        }


        protected override void Act()
        {
            var mock = new Mock<FlyingObject>(_id, _code, _currentAirport);
            _sut = mock.Object;
        }


        [Test]
        public void Then_It_Should_Create_A_Valid_Instance()
        {
            Assert.IsNotNull(_sut);
        }


        [Test]
        public void Then_It_Should_Have_Correct_Id()
        {
            Assert.AreEqual(_id, _sut.Id);
        }


        [Test]
        public void Then_It_Should_Have_Correct_Code()
        {
            Assert.AreEqual(_code, _sut.Code);
        }


        [Test]
        public void Then_It_Should_Have_Correct_Current_Airport()
        {
            Assert.AreEqual(_currentAirport, _sut.CurrentAirport);
        }
    }


    [TestFixture]
    public abstract class Given_Valid_Dependencies_And_Constructed_Instance_Scenario
        : ArrangeActAssertTest
    {
        protected FlyingObject _sut;



        protected override void Arrange()
        {
            const int id = 666;
            const string code = "123-ABC";
            var currentAirport = new Airport("BRU");

            var mock = new Mock<FlyingObject>(id, code, currentAirport);
            _sut = mock.Object;
        }
    }



    [TestFixture]
    public class Given_FlightRoute_When_Taking_Off
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private FlightRoute _flightRoute;


        protected override void Arrange()
        {
            base.Arrange();

            var departure = new Airport("a");
            var destination = new Airport("b");
            var flightRouteValidationService = new DifferentAirportFlightRouteValidationService();
            var distanceCalculator = Mock.Of<IDistanceCalculator>();

            _flightRoute = FlightRoute.Create(departure, destination, flightRouteValidationService, distanceCalculator);
        }


        protected override void Act()
        {
            _sut.TakeOff(_flightRoute);
        }


        [Test]
        public void Then_It_Should_Have_Null_Airport_As_Current_Airport()
        {
            Assert.AreEqual(Airport.Null, _sut.CurrentAirport);
        }


        [Test]
        public void Then_It_Should_Have_Incremented_The_TotalFlights()
        {
            Assert.AreEqual(1, _sut.TotalFlights);
        }
    }



    [TestFixture]
    public class Given_Flying_When_Landing
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private FlightRoute _flightRoute;
        private Airport _destination;



        protected override void Arrange()
        {
            base.Arrange();

            var departure = new Airport("a");
            _destination = new Airport("b");
            var flightRouteValidationService = new DifferentAirportFlightRouteValidationService();

            var distanceCalculatorMock = new Mock<IDistanceCalculator>();
            var distanceCalculator = distanceCalculatorMock.Object;

            distanceCalculatorMock
                .Setup(x => x.Calculate(departure, _destination))
                .Returns(123d);


            _flightRoute = FlightRoute.Create(departure, _destination, flightRouteValidationService, distanceCalculator);

            _sut.TakeOff(_flightRoute);
        }


        protected override void Act()
        {
            _sut.Land();
        }


        [Test]
        public void Then_It_Should_Have_Destination_Airport_As_Current_Airport()
        {
            Assert.AreEqual(_destination, _sut.CurrentAirport);
        }


        [Test]
        public void Then_It_Should_Have_Incremented_AirMiles()
        {
            Assert.AreEqual(_flightRoute.Distance, _sut.AirMiles);
        }
    }
}