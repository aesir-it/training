﻿using FlightController.Domain.Model;
using NUnit.Framework;
using TestSupport;


// ReSharper disable once CheckNamespace
namespace FlightController.Domain.Test.Model.AirportTest
{
    [TestFixture]
    public class Given_Valid_Dependencies_When_Constructing_Instance
        : ArrangeActAssertTest
    {
        private Airport _sut;
        private string _code;



        protected override void Arrange()
        {
            _code = "BRU";
        }


        protected override void Act()
        {
            _sut = new Airport(_code);
        }


        [Test]
        public void Then_It_Should_Create_A_Valid_Instance()
        {
            Assert.IsNotNull(_sut);
        }


        [Test]
        public void Then_It_Should_Have_Correct_Code()
        {
            Assert.AreEqual(_code, _sut.Code);
        }
    }



    [TestFixture]
    public class Given_An_Airport_When_Comparing_To_An_Airport_With_The_Same_Code
        : ArrangeActAssertTest
    {
        private Airport _sut;
        private string _code;
        private Airport _otherAirport;
        private bool _result;



        protected override void Arrange()
        {
            _code = "ABC";

            _sut = new Airport(_code);

            _otherAirport = new Airport(_code);
        }


        protected override void Act()
        {
            _result = _sut == _otherAirport;
        }


        [Test]
        public void Then_It_Should_Be_The_Same_Airport()
        {
            Assert.IsTrue(_result);
        }
    }
}