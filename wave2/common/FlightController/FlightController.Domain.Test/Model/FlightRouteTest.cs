﻿using System;
using FlightController.Domain.Calculators.Contracts;
using FlightController.Domain.Model;
using FlightController.Domain.Services.Contracts;
using Moq;
using NUnit.Framework;
using TestSupport;


// ReSharper disable once CheckNamespace
namespace FlightController.Domain.Test.Model.FlightRouteTest
{
    [TestFixture]
    public class Given_Valid_Dependencies_When_Constructing_Instance
        : ArrangeActAssertTest
    {
        private FlightRoute _sut;
        private Airport _departure;
        private Airport _destination;
        private IFlightRouteValidationService _flightRouteValidationService;
        private Mock<IDistanceCalculator> _distanceCalculatorMock;
        private IDistanceCalculator _distanceCalculator;



        private const double Distance = 123d;



        protected override void Arrange()
        {
            _departure = new Airport("One");
            _destination = new Airport("Two");

            _flightRouteValidationService = Mock.Of<IFlightRouteValidationService>();

            _distanceCalculatorMock = new Mock<IDistanceCalculator>();
            _distanceCalculator = _distanceCalculatorMock.Object;

            _distanceCalculatorMock
                .Setup(x => x.Calculate(_departure, _destination))
                .Returns(Distance);
        }


        protected override void Act()
        {
            _sut = FlightRoute.Create(_departure, _destination, _flightRouteValidationService, _distanceCalculator);
        }


        [Test]
        public void Then_It_Should_Create_A_Valid_Instance()
        {
            Assert.IsNotNull(_sut);
        }


        [Test]
        public void Then_It_Should_Have_Correct_Departure()
        {
            Assert.AreEqual(_departure, _sut.Departure);
        }


        [Test]
        public void Then_It_Should_Have_Correct_Destination()
        {
            Assert.AreEqual(_destination, _sut.Destination);
        }


        [Test]
        public void Then_It_Should_Have_Correct_Distance()
        {
            Assert.AreEqual(Distance, _sut.Distance);
        }


        [Test]
        public void Then_It_Should_Have_Calculated_The_Distance_Using_DistanceCalculator()
        {
            _distanceCalculatorMock.Verify(x => x.Calculate(_departure, _destination), Times.Once);
        }
    }



    [TestFixture]
    public class Given_Departure_And_Destination_Are_The_Same_When_Constructing_Instance
        : ArrangeActAssertTest
    {
        private Airport _departure;
        private Airport _destination;
        private FuncExtensions.FuncResult<FlightRoute> _result;
        private Mock<IFlightRouteValidationService> _flightRouteValidationServiceMock;
        private IFlightRouteValidationService _flightRouteValidationService;
        private Mock<IDistanceCalculator> _distanceCalculatorMock;
        private IDistanceCalculator _distanceCalculator;



        protected override void Arrange()
        {
            _departure = new Airport("One");
            _destination = new Airport("One");

            _flightRouteValidationServiceMock = new Mock<IFlightRouteValidationService>();
            _flightRouteValidationService = _flightRouteValidationServiceMock.Object;

            _flightRouteValidationServiceMock
                .Setup(x => x.Validate(_departure, _destination))
                .Throws<Exception>();

            _distanceCalculatorMock = new Mock<IDistanceCalculator>();
            _distanceCalculator = _distanceCalculatorMock.Object;
        }


        protected override void Act()
        {
            Func<FlightRoute> flightRouteFunc =
                () => FlightRoute.Create(_departure, _destination, _flightRouteValidationService, _distanceCalculator);

            _result = flightRouteFunc.ExecuteWithCatch();
        }


        [Test]
        public void Then_It_Should_Not_Create_A_Valid_Instance()
        {
            Assert.IsNull(_result.Result);
        }


        [Test]
        public void Then_It_Should_Have_Thrown_Exception()
        {
            Assert.IsInstanceOf<Exception>(_result.ExceptionCatcher.Exception);
        }


        [Test]
        public void Then_It_Should_Have_Validated_Using_FlightRouteValidationService()
        {
            _flightRouteValidationServiceMock.Verify(x => x.Validate(_departure, _destination), Times.Once);
        }


        [Test]
        public void Then_It_Should_Not_Have_Calculated_The_Distance_Using_DistanceCalculator()
        {
            _distanceCalculatorMock.Verify(x => x.Calculate(_departure, _destination), Times.Never);
        }
    }
}