﻿using System;
using System.Collections.Generic;
using FlightController.Domain.Calculators.Contracts;
using FlightController.Domain.Model;
using FlightController.Domain.Services;
using Moq;
using NUnit.Framework;
using TestSupport;


// ReSharper disable once CheckNamespace
namespace FlightController.Domain.Test.Services.FlightControllerServiceTest
{
    [TestFixture]
    public class Given_Valid_Dependencies_When_Constructing_Instance
        : ArrangeActAssertTest
    {
        private FlightControllerService _sut;
        private List<FlyingObject> _flyingObjects;
        private List<FlightRoute> _flightRoutes;



        protected override void Arrange()
        {
            _flyingObjects = new List<FlyingObject>();
            _flightRoutes = new List<FlightRoute>();
        }


        protected override void Act()
        {
            _sut = new FlightControllerService(_flyingObjects, _flightRoutes);
        }


        [Test]
        public void Then_It_Should_Create_A_Valid_Instance()
        {
            Assert.IsNotNull(_sut);
        }
    }



    [TestFixture]
    public abstract class Given_Valid_Dependencies_And_Constructed_Instance_Scenario
        : ArrangeActAssertTest
    {
        protected FlightControllerService _sut;



        protected override void Arrange()
        {
            var flyingObjects = CreateFlyingObject();
            var flightRoutes = CreateFlightRoutes();

            _sut = new FlightControllerService(flyingObjects, flightRoutes);
        }


        protected abstract List<FlyingObject> CreateFlyingObject();


        protected abstract List<FlightRoute> CreateFlightRoutes();
    }



    [TestFixture]
    public class Given_No_FlyingObjects_And_No_FlightRoutes_When_Running
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private FlightRunResult _result;



        protected override void Act()
        {
            _result = _sut.Run();
        }


        [Test]
        public void Then_It_Should_Return_Valid_Result()
        {
            Assert.IsNotNull(_result);
        }


        [Test]
        public void Then_It_Should_Have_Correct_TotalFlightCount()
        {
            Assert.AreEqual(0, _result.TotalFlightCount);
        }


        [Test]
        public void Then_It_Should_Have_Correct_TotalDuration()
        {
            Assert.Greater(_result.TotalDuration, 0d);
        }


        protected override List<FlyingObject> CreateFlyingObject() => new List<FlyingObject>();


        protected override List<FlightRoute> CreateFlightRoutes() => new List<FlightRoute>();
    }



    [TestFixture]
    public class Given_One_FlyingObject_And_Three_FlightRoutes_When_Running
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private FuncExtensions.FuncResult<FlightRunResult> _executionResult;
        private IDistanceCalculator _distanceCalculator;



        protected override void Arrange()
        {
            var distanceCalculatorMock = new Mock<IDistanceCalculator>();
            _distanceCalculator = distanceCalculatorMock.Object;

            distanceCalculatorMock
                .Setup(x => x.Calculate(new Airport("BRU", ""), new Airport("TFS", "")))
                .Returns(3000d);

            distanceCalculatorMock
                .Setup(x => x.Calculate(new Airport("TFS", ""), new Airport("ALC", "")))
                .Returns(1000d);

            distanceCalculatorMock
                .Setup(x => x.Calculate(new Airport("ALC", ""), new Airport("OST", "")))
                .Returns(2000d);

            base.Arrange();
        }


        protected override void Act()
        {
            Func<FlightRunResult> func = _sut.Run;
            _executionResult = func.ExecuteWithCatch();
        }


        protected override List<FlyingObject> CreateFlyingObject() =>
            new List<FlyingObject>
            {
                new Airplane(1, "Airforce 1", new Airport("BRU"))
            };


        protected override List<FlightRoute> CreateFlightRoutes()
        {
            var differentAirportFlightRouteValidationService = new DifferentAirportFlightRouteValidationService();

            return
                new List<FlightRoute>
                {
                    FlightRoute.Create(new Airport("BRU"), new Airport("TFS"),
                        differentAirportFlightRouteValidationService, _distanceCalculator),
                    FlightRoute.Create(new Airport("TFS"), new Airport("ALC"),
                        differentAirportFlightRouteValidationService, _distanceCalculator),
                    FlightRoute.Create(new Airport("ALC"), new Airport("OST"),
                        differentAirportFlightRouteValidationService, _distanceCalculator)
                };
        }


        [Test]
        public void Then_It_Should_Return_Valid_Result()
        {
            Assert.IsNotNull(_executionResult.Result);
        }


        [Test]
        public void Then_It_Should_Have_Correct_TotalFlightCount()
        {
            Assert.AreEqual(3, _executionResult.Result.TotalFlightCount);
        }


        [Test]
        public void Then_It_Should_Have_Correct_TotalDuration()
        {
            Assert.Greater(_executionResult.Result.TotalDuration, 0d);
        }


        [Test]
        public void Then_It_Should_Not_Throw_An_Invalid_Operation_Exception()
        {
            Assert.IsNotInstanceOf<InvalidOperationException>(_executionResult.ExceptionCatcher.Exception);
        }
    }
}