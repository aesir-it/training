﻿namespace FlightController.Application.Model.Results
{
    public class AirportCreationResult
    {
        public ResultStatus Status { get; private set; }



        public AirportCreationResult(ResultStatus status)
        {
            Status = status;
        }
    }
}