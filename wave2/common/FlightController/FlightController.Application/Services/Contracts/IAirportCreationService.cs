﻿using FlightController.Application.Model.Results;


namespace FlightController.Application.Services.Contracts
{
    public interface IAirportCreationService
    {
        AirportCreationResult Create(string code, string name);
    }
}