﻿using FlightController.Application.Model.Results;
using FlightController.Application.Services.Contracts;
using FlightController.Domain.Model;
using FlightController.Domain.Repositories.Contracts;


namespace FlightController.Application.Services
{
    public class AirportCreationService
        : IAirportCreationService
    {
        private readonly IAirportRepository _airportRepository;



        public AirportCreationService(IAirportRepository airportRepository)
        {
            _airportRepository = airportRepository;
        }


        public AirportCreationResult Create(string code, string name)
        {
            var airport = new Airport(code, name);

            _airportRepository.Create(airport);

            return new AirportCreationResult(ResultStatus.Ok);
        }

    }
}