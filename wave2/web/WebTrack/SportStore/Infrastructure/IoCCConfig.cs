using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;


namespace SportStore.Infrastructure
{
    public static class IoCCConfig
    {
        public static void Register()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule<ControllerModule>();
            containerBuilder.RegisterModule<RepositoryModule>();

            var container = containerBuilder.Build();


            var dependencyResolver = new AutofacDependencyResolver(container);

            DependencyResolver.SetResolver(dependencyResolver);
        }
    }
}