using Autofac;
using Domain.Infra.EF.Repositories;
using SportStore.Domain.Repositories.Contracts;


namespace SportStore.Infrastructure
{
    public class RepositoryModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<ProductRepository>()
                //.RegisterType<InMemoryProductRepository>()
                .As<IProductRepository>();
        }
    }
}