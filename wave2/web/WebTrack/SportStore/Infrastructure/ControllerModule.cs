using Autofac;
using SportStore.Controllers;


namespace SportStore.Infrastructure
{
    public class ControllerModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<ProductController>()
                .AsSelf();

            builder
                .RegisterType<NavigationController>()
                .AsSelf();
        }
    }
}