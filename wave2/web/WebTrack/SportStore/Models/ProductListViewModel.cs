﻿using System.Collections.Generic;
using SportStore.Domain.Model;


namespace SportStore.Models
{
    public class ProductListViewModel
    {
        public IEnumerable<Product> Products { get; set; }


        public PagingInfoViewModel PagingInfo { get; set; }


        public string CurrentCategory { get; set; }
    }
}