﻿using System;

namespace SportStore.Models
{
    public class PagingInfoViewModel
    {
        public int TotalItems { get; set; }


        public int ItemsPerPage { get; set; }


        public int CurrentPage { get; set; }


        public int TotalPages => (int) Math.Ceiling((double) TotalItems / ItemsPerPage);
    }
}