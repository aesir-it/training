﻿namespace SportStore.Models
{
    // This is actually returned from our server and we cannot mutate it.
    public class ProductDto
    {
        public int Id { get; set; }


        public decimal Price { get; set; }


        public string Category { get; set; }
    }



    // Pure: map to vm.
    public class SomeProductMappedViewModel
    {
        public int ProductId { get; set; }


        public decimal Price { get; set; }


        public string Category { get; set; }


        public string FriendlyCatalogName => $"Id: {ProductId} -- €{Price:00.00}";
    }


    public class SomeProductMappedViewModelMapper
    {
        public SomeProductMappedViewModel Map(ProductDto dto)
        {
            var vm2 = new SomeProductContained2ViewModel(dto);
            vm2.Price = 666m;

            return
                new SomeProductMappedViewModel
                {
                    ProductId = dto.Id,
                    Price = dto.Price,
                    Category = dto.Category
                };

            // Automapper.
            //var vm = autoMapper.Map<SomeProductMappedViewModel>(dto);
        }
    }



    // Pure: contain variant 1
    public class SomeProductContained1ViewModel
    {
        public int ProductId { get; set; }


        public decimal Price { get; set; }


        public string Category { get; set; }


        public string FriendlyCatalogName => $"Id: {ProductId} -- €{Price:00.00}";



        public SomeProductContained1ViewModel(ProductDto dto)
        {
            ProductId = dto.Id;
            Price = dto.Price;
            Category = dto.Category;
        }
    }



    // Pure: contain variant 2
    public class SomeProductContained2ViewModel
    {
        private readonly ProductDto _dto;



        // Not settable.
        public int ProductId => _dto.Id;


        // Allows set.
        public decimal Price
        {
            get { return _dto.Price; }
            set { _dto.Price = value; }
        }


        public string Category => _dto.Category;


        public string FriendlyCatalogName => $"Id: {ProductId} -- €{Price:00.00}";



        public SomeProductContained2ViewModel(ProductDto dto)
        {
            _dto = dto;
        }
    }



    // Pragmatic
    public class SomeProductPragmaticViewModel
    {
        public ProductDto ProductDto { get; }



        public string FriendlyCatalogName => $"Id: {ProductDto.Id} -- €{ProductDto.Price:00.00}";



        public SomeProductPragmaticViewModel(ProductDto dto)
        {
            ProductDto = dto;
        }
    }
}