﻿using System;
using System.Text;
using System.Web.Mvc;
using SportStore.Models;


namespace SportStore.HtmlHelpers
{
    public static class PagingHelpers
    {
        public static MvcHtmlString PageLinks(this HtmlHelper html, PagingInfoViewModel vm, Func<int, string> pageLinkCalc)
        {
            var sb = new StringBuilder();

            for (var i = 1; i <= vm.TotalPages; i++)
            {
                var tag = new TagBuilder("a");

                var pageLink = pageLinkCalc(i);
                tag.MergeAttribute("href", pageLink);
                tag.InnerHtml = $"{i}";
                tag.AddCssClass("btn btn-default");

                if (i == vm.CurrentPage)
                {
                    tag.AddCssClass("btn-primary");
                    tag.AddCssClass("selected");
                }

                sb.Append(tag);
            }

            return MvcHtmlString.Create(sb.ToString());
        }
    }
}