﻿using System.Reflection;
using System.Text;
using System.Web.Mvc;


namespace SportStore.HtmlHelpers
{
    public static class VarDumpHelper
    {
        public static MvcHtmlString VarDump(this HtmlHelper html, object obj)
        {
            var sb = new StringBuilder();

            var rootType = obj.GetType();

            var members = rootType.GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            foreach (var memberInfo in members)
            {
                if (memberInfo.MemberType != MemberTypes.Property)
                {
                    continue;
                }
                var propertyInfo = (PropertyInfo) memberInfo;

                sb.AppendLine($"{propertyInfo.Name} ({propertyInfo.PropertyType}) => {propertyInfo.GetValue(obj)}");
            }

            return MvcHtmlString.Create(sb.ToString());
        }
    }
}