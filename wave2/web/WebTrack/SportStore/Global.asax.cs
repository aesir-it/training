﻿using System.Web.Mvc;
using System.Web.Routing;
using SportStore.Infrastructure;


namespace SportStore
{
    public class MvcApplication
        : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            IoCCConfig.Register();
        }
    }
}