﻿using System.Web.Mvc;
using SportStore.Domain.Repositories.Contracts;


namespace SportStore.Controllers
{
    public class NavigationController
        : Controller
    {
        private readonly IProductRepository _productRepository;



        public NavigationController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }


        public PartialViewResult Menu(string category = null)
        {
            ViewBag.SelectedCategory = category;

            var categories = _productRepository.GetAllCategories();

            return PartialView(categories);
        }
    }
}