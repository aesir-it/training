﻿using System.Web.Mvc;
using SportStore.Domain.Repositories.Contracts;
using SportStore.Models;


namespace SportStore.Controllers
{
    public class ProductController
        : Controller
    {
        private readonly IProductRepository _productRepository;


        public int _pageSize = 2;



        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }


        public ActionResult List(string category, int page = 1)
        {
            //var products = _productRepository.GetAll();
            var products = _productRepository.GetAll(category, page, _pageSize);

            var totalItems = _productRepository.GetTotalCount(category);

            var pagingInfo =
                new PagingInfoViewModel
                {
                    TotalItems = totalItems,
                    ItemsPerPage = _pageSize,
                    CurrentPage = page
                };

            var productListViewModel =
                new ProductListViewModel
                {
                    Products = products,
                    PagingInfo = pagingInfo,
                    CurrentCategory = category
                };

            return View(productListViewModel);
        }
    }
}