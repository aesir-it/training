﻿using System;
using System.Web.Mvc;
using PartyInvites.Models;


namespace PartyInvites.Controllers
{
    public class HomeController
        : Controller
    {
        // GET: Home
        public ViewResult Index()
        {
            var hour = DateTime.Now.Hour;

            ViewBag.Greeting = hour < 12 ? "Good morning" : "Good afternoon";

            return View();
        }


        [HttpGet]
        public ActionResult Respond()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Respond(GuestResponse guestResponse)
        {
            if (ModelState.IsValid)
            {
                return View("Thanks", guestResponse);
            }

            return View();
        }
    }
}