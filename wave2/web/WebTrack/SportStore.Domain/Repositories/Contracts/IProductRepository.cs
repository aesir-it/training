﻿using System.Collections.Generic;
using SportStore.Domain.Model;


namespace SportStore.Domain.Repositories.Contracts
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetAll();


        IEnumerable<Product> GetAll(int page, int pageSize);


        IEnumerable<Product> GetAll(string category, int page, int pageSize);


        int GetTotalCount();


        int GetTotalCount(string category);


        IEnumerable<string> GetAllCategories();
    }
}