﻿using System.Data.Entity;
using SportStore.Domain.Model;


namespace Domain.Infra.EF.UnitOfWork
{
    public class SportStoreContext
        : DbContext
    {
        public DbSet<Product> Products { get; set; }
    }
}