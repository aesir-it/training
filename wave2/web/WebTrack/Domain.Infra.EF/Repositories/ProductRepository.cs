﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Infra.EF.UnitOfWork;
using SportStore.Domain.Model;
using SportStore.Domain.Repositories.Contracts;


namespace Domain.Infra.EF.Repositories
{
    public class ProductRepository
        : IProductRepository
    {
        public IEnumerable<Product> GetAll()
        {
            using (var dbContext = new SportStoreContext())
            {
                return
                    GetAllAsQueryable(dbContext)
                        .ToList();
            }
        }


        public IEnumerable<Product> GetAll(int page, int pageSize)
        {
            using (var dbContext = new SportStoreContext())
            {
                var all = GetAllAsQueryable(dbContext);

                return
                    GetPaged(all, page, pageSize)
                        .ToList();
            }
            //using (var dbContext = new SportStoreContext())
            //{
            //    return
            //        dbContext
            //            .Products
            //            .OrderBy(x => x.ProductId)
            //            .Skip((page - 1) * pageSize)
            //            .Take(pageSize)
            //            .ToList();
            //}
        }


        public IEnumerable<Product> GetAll(string category, int page, int pageSize)
        {
            using (var dbContext = new SportStoreContext())
            {
                return
                    GetAllForCategoryPaged(dbContext, category, page, pageSize)
                        .ToList();
            }
            //using (var dbContext = new SportStoreContext())
            //{
            //    return
            //        dbContext
            //            .Products
            //            .Where(x => String.IsNullOrWhiteSpace(category) || x.Category == category)
            //            .OrderBy(x => x.ProductId)
            //            .Skip((page - 1) * pageSize)
            //            .Take(pageSize)
            //            .ToList();
            //}
        }


        private IQueryable<Product> GetAllAsQueryable(SportStoreContext dbContext)
        {
            return dbContext.Products;
        }


        private IQueryable<Product> GetPaged(IQueryable<Product> queryable, int page, int pageSize)
        {
            return
                queryable
                    .OrderBy(x => x.ProductId)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize);
        }


        private IQueryable<Product> GetAllForCategoryPaged(SportStoreContext dbContext, string category, int page, int pageSize)
        {
            var all = GetAllAsQueryable(dbContext);

            var filteredQueryable =
                GetAllForCategory(all, category);

            return GetPaged(filteredQueryable, page, pageSize);
        }


        private IQueryable<Product> GetAllForCategory(IQueryable<Product> queryable, string category)
        {
            var trimmedCategory = category?.Trim();

            return
                queryable
                    .Where(x => trimmedCategory == null || trimmedCategory == "" || x.Category == trimmedCategory);
        }


        public int GetTotalCount()
        {
            using (var dbContext = new SportStoreContext())
            {
                return GetAllAsQueryable(dbContext).Count();
            }
        }


        public int GetTotalCount(string category)
        {
            using (var dbContext = new SportStoreContext())
            {
                var all = GetAllAsQueryable(dbContext);

                return GetAllForCategory(all, category).Count();
            }
        }


        public IEnumerable<string> GetAllCategories()
        {
            using (var dbContext = new SportStoreContext())
            {
                var all = GetAllAsQueryable(dbContext);

                return
                    all
                        .Select(x => x.Category)
                        .Distinct()
                        .ToList();
            }
        }
    }
}