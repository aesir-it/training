﻿using System.Web.Mvc;
using RazorFun.Models;


namespace RazorFun.Controllers
{
    public class HomeController
        : Controller
    {
        private Product _product;



        public HomeController()
        {
            _product =
                new Product
                {
                    ProductId = 1,
                    Name = "Bike",
                    Description = "Something fun to ride with",
                    Price = 1500m,
                    Category = "Cycling"
                };
        }


        // GET: Home
        public ActionResult Index()
        {
            return View(_product);
        }


        public ActionResult NameAndPrice()
        {
            return View(_product);
        }


        public ActionResult DemoExpression()
        {
            ViewBag.ProductCount = 1;
            ViewBag.ApplyDiscount = true;
            ViewBag.ExpressShip = false;

            return View(_product);
        }


        public ActionResult DemoArray()
        {
            var products =
                new[]
                {
                    new Product { Name="Bike", Price = 123m },
                    new Product { Name="Gocart", Price = 450m },
                    new Product { Name="Skateboard", Price = 100m },
                };

            return View(products);
        }
    }
}