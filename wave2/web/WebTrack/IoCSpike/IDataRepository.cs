﻿namespace IoCSpike
{
    public interface IDataRepository
    {
        string GetData();
    }
}