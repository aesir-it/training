﻿using System;
using Autofac;

namespace IoCSpike
{
    class Program
    {
        static void Main(string[] args)
        {
            //var dataRepository = new MemoryDataRepository();

            var containerBuilder = new ContainerBuilder();

            containerBuilder
                .RegisterType<DatabaseRepository>()
                .As<IDataRepository>();


            containerBuilder
                .RegisterType<FakeDbContext>()
                .As<IDbContext>();

            containerBuilder
                .RegisterType<MemoryDataRepository>()
                .As<IDataRepository>()
                .PreserveExistingDefaults();

            containerBuilder
                .RegisterType<HomeController>()
                //.As<HomeController>()
                .AsSelf();

            var container = containerBuilder.Build();

            var homeController = container.Resolve<HomeController>();


            //IDbContext dbContext = new FakeDbContext();

            //IDataRepository dataRepository = new DatabaseRepository(dbContext);

            //var homeController = new HomeController(dataRepository);

            var result = homeController.Index();

            Console.WriteLine(result);
            Console.ReadKey();

            container.Dispose();

            Console.ReadKey();
        }
    }


    public class HomeController
    {
        private readonly IDataRepository _dataRepository;



        public HomeController(IDataRepository dataRepository)
        {
            _dataRepository = dataRepository;
            //_dataRepository = new MemoryDataRepository();
        }


        public string Index()
        {
            var data = _dataRepository.GetData();

            return data;
        }
    }

    public class MemoryDataRepository
        : IDataRepository
    {
        public MemoryDataRepository()
        {
        }


        public string GetData()
        {
            return "Somedata";
        }
    }


    public class DatabaseRepository
        : IDataRepository
    {
        private readonly IDbContext _context;


        public DatabaseRepository(IDbContext context)
        {
            _context = context;
        }


        public DatabaseRepository()
        {
        }


        public string GetData()
        {
            var data = _context?.GetData();

            return data + "extra";
        }
    }

    public interface IDbContext
    {
        string GetData();
    }

    public class FakeDbContext
        : IDbContext, IDisposable
    {
        public FakeDbContext()
        {
        }


        public string GetData()
        {
            return "some fake db data";
        }


        public void Dispose()
        {
            Console.WriteLine("Disposing...");
        }
    }
}