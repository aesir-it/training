﻿using NUnit.Framework;
using SportStore.Models;


// ReSharper disable once CheckNamespace
namespace SportStore.Test.Models.PagingInfoViewModelTest
{
    [TestFixture]
    public abstract class Given_Total_Items_And_Items_Per_Page_When_Calculating_Total_Pages_Scenario
         : ArrangeActAssertTest
    {
        private PagingInfoViewModel _sut;
        private int _result;



        protected abstract int TotalItems { get; }


        protected abstract int ItemsPerPage { get; }


        protected abstract int ExpectedTotalPages { get; }



        protected override void Arrange()
        {
            _sut =
                new PagingInfoViewModel
                {
                    TotalItems = TotalItems,
                    ItemsPerPage = ItemsPerPage
                };
        }


        protected override void Act()
        {
            _result = _sut.TotalPages;
        }


        [Test]
        public void Then_It_Should_Have_Calculated_Correct_Number_Of_Pages()
        {
            Assert.AreEqual(ExpectedTotalPages, _result);
        }
    }



    [TestFixture]
    public class Given_20_Items_And_5_Items_Per_Page
        : Given_Total_Items_And_Items_Per_Page_When_Calculating_Total_Pages_Scenario
    {
        protected override int TotalItems => 20;


        protected override int ItemsPerPage => 5;


        protected override int ExpectedTotalPages => 4;
    }



    [TestFixture]
    public class Given_17_Items_And_5_Items_Per_Page
        : Given_Total_Items_And_Items_Per_Page_When_Calculating_Total_Pages_Scenario
    {
        protected override int TotalItems => 17;


        protected override int ItemsPerPage => 5;


        protected override int ExpectedTotalPages => 4;
    }



    [TestFixture]
    public class Given_17_Items_And_3_Items_Per_Page
        : Given_Total_Items_And_Items_Per_Page_When_Calculating_Total_Pages_Scenario
    {
        protected override int TotalItems => 17;


        protected override int ItemsPerPage => 3;


        protected override int ExpectedTotalPages => 6;
    }
}