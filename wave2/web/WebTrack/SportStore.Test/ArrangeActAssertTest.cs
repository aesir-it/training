﻿using NUnit.Framework;


namespace SportStore.Test
{
    [TestFixture]
    public abstract class ArrangeActAssertTest
    {
        [SetUp]
        public void Setup()
        {
            Arrange();
            Act();
        }


        protected abstract void Arrange();


        protected abstract void Act();
    }
}