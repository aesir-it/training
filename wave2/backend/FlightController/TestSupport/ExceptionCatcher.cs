﻿using System;


namespace TestSupport
{
    public class ExceptionCatcher
    {
        public Exception Exception { get; private set; }



        public void Execute(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                Exception = ex;
            }
        }
    }
}