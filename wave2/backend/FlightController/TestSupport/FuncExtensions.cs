using System;


namespace TestSupport
{
    public static class FuncExtensions
    {
        public static FuncResult<T> ExecuteWithCatch<T>(this Func<T> func)
        {
            var exceptionCatcher = new ExceptionCatcher();

            var result = default (T);

            exceptionCatcher.Execute(() => result = func());

            var funcResult = new FuncResult<T>(result, exceptionCatcher);

            return funcResult;
        }



        public class FuncResult<T>
        {
            public T Result { get; private set; }


            public ExceptionCatcher ExceptionCatcher { get; private set; }



            public FuncResult(T result, ExceptionCatcher exceptionCatcher)
            {
                Result = result;
                ExceptionCatcher = exceptionCatcher;
            }
        }
    }
}