using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace FlightController.Infra.EF.Model
{
    [Table("Airport")]
    public class AirportPersistenceModel
    {
        [Key]
        public string Code { get; set; }


        public string Name { get; set; }
    }
}