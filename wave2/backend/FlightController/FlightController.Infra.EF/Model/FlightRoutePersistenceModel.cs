﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace FlightController.Infra.EF.Model
{
    [Table("FlightRoute")]
    public class FlightRoutePersistenceModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FlightRouteId { get; set; }


        [ForeignKey("Departure")]
        public string DepartureCode { get; set; }


        //[ForeignKey("Code")]
        public AirportPersistenceModel Departure { get; set; }


        [ForeignKey("Destination")]
        public string DestinationCode { get; set; }


        //[ForeignKey("Code")]
        public AirportPersistenceModel Destination { get; set; }
    }
}