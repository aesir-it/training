﻿using System.Data.Entity;
using FlightController.Infra.EF.Model;


namespace FlightController.Infra.EF.UnitOfWork
{
    public class FlightControllerContext
        : DbContext
    {
        public DbSet<AirportPersistenceModel> Airports { get; set; }


        public DbSet<FlightRoutePersistenceModel> FlightRoutes { get; set; }



        public FlightControllerContext()
            : base("FlightController")
        {
        }
    }
}