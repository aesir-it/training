﻿using System;
using System.Linq;
using FlightController.Domain.Model;
using FlightController.Domain.Repositories.Contracts;
using FlightController.Infra.EF.Model;
using FlightController.Infra.EF.UnitOfWork;


namespace FlightController.Infra.EF.Repositories
{
    public class AirportEFRepository
        : IAirportRepository
    {
        public void Create(Airport airport)
        {
            using (var context = new FlightControllerContext())
            {
                var model =
                    context
                        .Airports
                        .Find(airport.Code);

                if (model != null)
                {
                    throw new Exception($"Airport {airport.Code} already exists");
                }

                var airportPersistanceModel =
                    new AirportPersistenceModel
                    {
                        Code = airport.Code,
                        Name = airport.Name
                    };

                context
                    .Airports
                    .Add(airportPersistanceModel);

                context.SaveChanges();
            }
        }


        public Airport Get(string code)
        {
            using (var context = new FlightControllerContext())
            {
                var model =
                    context
                        .Airports
                        .Single(x => x.Code == code);

                return new Airport(model.Code, model.Name);
            }
        }
    }
}