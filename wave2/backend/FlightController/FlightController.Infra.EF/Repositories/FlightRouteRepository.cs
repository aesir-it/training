﻿using System;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using FlightController.Domain.Calculators;
using FlightController.Domain.Model;
using FlightController.Domain.Repositories.Contracts;
using FlightController.Domain.Services;
using FlightController.Infra.EF.Model;
using FlightController.Infra.EF.UnitOfWork;


namespace FlightController.Infra.EF.Repositories
{
    public class FlightRouteRepository
        : IFlightRouteRepository
    {
        public void Create(FlightRoute flightRoute)
        {
            using (var dbContext = new FlightControllerContext())
            {
                var existingFlightRoute = Find(flightRoute.Departure, flightRoute.Destination, dbContext);
                if (existingFlightRoute != null)
                {
                    throw new Exception("exists");
                }

                // Doesn't work: will create airports again due to not attachted to context.
                //var model =
                //    new FlightRoutePersistenceModel
                //    {
                //        Departure =
                //            new AirportPersistenceModel
                //            {
                //                Code = flightRoute.Departure.Code,
                //                Name = flightRoute.Departure.Name
                //            },
                //        Destination =
                //            new AirportPersistenceModel
                //            {
                //                Code = flightRoute.Destination.Code,
                //                Name = flightRoute.Destination.Name
                //            }
                //    };


                var departure =
                    dbContext
                        .Airports
                        .Single(x => x.Code == flightRoute.Departure.Code);

                var destination =
                    dbContext
                        .Airports
                        .Single(x => x.Code == flightRoute.Destination.Code);

                //var adapter = (IObjectContextAdapter) dbContext;
                //var objContext = adapter.ObjectContext;

                var model =
                    new FlightRoutePersistenceModel
                    {
                        //DepartureCode = departure.Code,
                        //DestinationCode = destination.Code
                        Departure = departure,
                        Destination =  destination
                    };

                dbContext
                    .FlightRoutes
                    .Add(model);

                dbContext.SaveChanges();
            }
        }


        public FlightRoute Get(Airport departure, Airport destination)
        {
            var flightRoute = Find(departure, destination);
            if (flightRoute == null)
            {
                throw new Exception("not found");
            }

            return flightRoute;
        }


        public FlightRoute Find(Airport departure, Airport destination)
        {
            using (var dbContext = new FlightControllerContext())
            {
                return Find(departure, destination, dbContext);
            }
        }


        public FlightRoute Find(Airport departure, Airport destination, FlightControllerContext dbContext)
        {
            var model =
                dbContext
                    .FlightRoutes
                    .Include("Departure")
                    .Include("Destination")
                    .SingleOrDefault(x => x.Departure.Code == departure.Code && x.Destination.Code == destination.Code);

            if (model == null)
            {
                return null;
            }

            var departureAirport = new Airport(model.Departure.Code, model.Departure.Name);
            var destinationAirport = new Airport(model.Destination.Code, model.Destination.Name);

            var flightRouteValidationService = new DifferentAirportFlightRouteValidationService();

            var distanceCalculator = new CrappyNonTestedDistanceCalculator();

            return
                FlightRoute.Create(departureAirport, destinationAirport, flightRouteValidationService, distanceCalculator);
        }
    }
}