﻿CREATE TABLE [dbo].[Airport] (
    [AirportId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Airport] PRIMARY KEY CLUSTERED ([AirportId] ASC)
);

