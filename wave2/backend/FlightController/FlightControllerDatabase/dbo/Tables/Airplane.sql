﻿CREATE TABLE [dbo].[Airplane] (
    [AirplaneId] INT NOT NULL,
    CONSTRAINT [PK_Airplane] PRIMARY KEY CLUSTERED ([AirplaneId] ASC),
    CONSTRAINT [FK_Airplane_FlyingObject] FOREIGN KEY ([AirplaneId]) REFERENCES [dbo].[FlyingObject] ([FlyingObjectId])
);

