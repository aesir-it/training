﻿using FlightController.Domain.Model;


namespace FlightController.Domain.Repositories.Contracts
{
    public interface IFlightRouteRepository
    {
        void Create(FlightRoute flightRoute);


        FlightRoute Get(Airport departure, Airport destination);


        FlightRoute Find(Airport departure, Airport destination);
    }
}