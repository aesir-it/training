﻿using System.Collections.Generic;
using FlightController.Domain.Model;


namespace FlightController.Domain.Repositories.Contracts
{
    public interface IFlightControllerRepository
    {
        IEnumerable<FlyingObject> GetAllFlyingObjects();


        IEnumerable<FlyingObject> GetFlyingObjectNeedingMaintenance();


        IEnumerable<FlyingObject> GetFlyingObjectsOnTheGround();


        void RemoveDueToInvalidFlightRoute(FlyingObject flyingObject);


        bool HasActiveFlyingObjects();
    }
}