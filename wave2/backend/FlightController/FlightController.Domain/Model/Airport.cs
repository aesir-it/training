﻿namespace FlightController.Domain.Model
{
    public class Airport
    {
        public string Code { get; }


        public string Name { get; }



        public static readonly Airport Null = new Airport("(null)");



        public Airport(string code, string name = "")
        {
            Code = code;
            Name = name;
        }


        protected bool Equals(Airport other)
        {
            return Code.Equals(other.Code);
        }


        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != GetType())
            {
                return false;
            }

            return Equals((Airport) obj);
        }


        public override int GetHashCode()
        {
            return Code?.GetHashCode() ?? 0;
        }


        public override string ToString()
        {
            return $"Code = {Code}";
        }


        public static bool operator ==(Airport left, Airport right)
        {
            return Equals(left, right);
        }


        public static bool operator !=(Airport left, Airport right)
        {
            return !Equals(left, right);
        }
    }
}