﻿using System;


namespace FlightController.Domain.Model.EventArguments
{
    public class StatusChangedEventArgs
        : EventArgs
    {
        public FlyingObjectStatus Status { get; }


        public FlyingObject FlyingObject { get; }



        public StatusChangedEventArgs(FlyingObjectStatus status, FlyingObject flyingObject)
        {
            Status = status;
            FlyingObject = flyingObject;
        }
    }
}