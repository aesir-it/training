﻿namespace FlightController.Domain.Model.Contracts
{
    public interface IFlyingObjectVisitor
    {
        void Visit(Airplane airplane);
    }
}