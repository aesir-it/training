﻿using FlightController.Domain.Model.Contracts;


namespace FlightController.Domain.Model
{
    public class Airplane
        : FlyingObject
    {
        public Airplane(int id, string code, Airport currentAirport)
            : base(id, code, currentAirport)
        {
        }


        public override void Visit(IFlyingObjectVisitor flyingObjectVisitor)
        {
            flyingObjectVisitor.Visit(this);
        }
    }
}