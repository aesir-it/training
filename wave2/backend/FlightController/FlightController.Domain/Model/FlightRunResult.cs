﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace FlightController.Domain.Model
{
    public class FlightRunResult
    {
        private readonly TimeSpan _delta;



        public List<FlyingObject> FlyingObjects { get; }


        public int TotalFlightCount => FlyingObjects.Sum(x => x.TotalFlights);


        public double TotalDuration => _delta.TotalMilliseconds;



        public FlightRunResult(List<FlyingObject> flyingObjects, TimeSpan delta)
        {
            _delta = delta;
            FlyingObjects = flyingObjects;
        }
    }
}