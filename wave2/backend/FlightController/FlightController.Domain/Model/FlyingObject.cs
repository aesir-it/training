﻿using System;
using FlightController.Domain.Model.Contracts;
using FlightController.Domain.Model.EventArguments;
using FlightController.Domain.Services.Contracts;


namespace FlightController.Domain.Model
{
    public abstract class FlyingObject
    {
        public event EventHandler<StatusChangedEventArgs> StatusChanged;



        private FlightRoute _flightRoute;
        private FlyingObjectStatus _status;



        public int Id { get; }


        public string Code { get; }


        public Airport CurrentAirport { get; private set; }


        public double AirMiles { get; private set; }


        public int TotalFlights { get; private set; }


        public int MaintenanceCount { get; private set; }


        public FlyingObjectStatus Status
        {
            get { return _status; }
            private set
            {
                _status = value;
                OnStatusChanged();
            }
        }


        protected FlyingObject(int id, string code, Airport currentAirport)
        {
            Id = id;
            Code = code;
            CurrentAirport = currentAirport;
        }


        public void TakeOff(FlightRoute flightRoute)
        {
            // REMARK: Null checks should be done here.

            Status = FlyingObjectStatus.Flying;
            CurrentAirport = Airport.Null;
            TotalFlights++;

            _flightRoute = flightRoute;
        }


        public void Land(IMaintenanceConditionService maintenanceConditionService, IRetirementConditionService retirementConditionService)
        {
            // REMARK: Null checks should be done here.

            Status = FlyingObjectStatus.OnTheGround;
            CurrentAirport = _flightRoute.Destination;
            AirMiles += _flightRoute.Distance;

            Visit(maintenanceConditionService);
            if (maintenanceConditionService.IsMaintenanceNeeded)
            {
                Status = FlyingObjectStatus.MaintenanceNeeded;
            }

            Visit(retirementConditionService);
            if (retirementConditionService.IsRetirementNeeded)
            {
                Status = FlyingObjectStatus.Retired;
            }
        }


        public void PerformMaintenance()
        {
            MaintenanceCount++;

            Status = FlyingObjectStatus.OnTheGround;
        }


        private void OnStatusChanged()
        {
            StatusChanged?.Invoke(this, new StatusChangedEventArgs(Status, this));
        }


        public abstract void Visit(IFlyingObjectVisitor flyingObjectVisitor);
    }
}