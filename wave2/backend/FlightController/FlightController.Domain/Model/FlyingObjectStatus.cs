﻿namespace FlightController.Domain.Model
{
    public enum FlyingObjectStatus
    {
        OnTheGround,
        Flying,
        MaintenanceNeeded,
        Retired
    }
}