﻿using FlightController.Domain.Repositories.Contracts;
using FlightController.Domain.Services.Contracts;


namespace FlightController.Domain.Services
{
    public class MaintenanceService
        : IMaintenanceService
    {
        private readonly IFlightControllerRepository _flightControllerRepository;



        public MaintenanceService(IFlightControllerRepository flightControllerRepository)
        {
            _flightControllerRepository = flightControllerRepository;
        }


        public void Run()
        {
            var flyingObjects = _flightControllerRepository.GetFlyingObjectNeedingMaintenance();

            foreach (var flyingObject in flyingObjects)
            {
                flyingObject.PerformMaintenance();
            }
        }
    }
}