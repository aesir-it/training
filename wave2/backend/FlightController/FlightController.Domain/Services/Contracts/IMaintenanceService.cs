﻿namespace FlightController.Domain.Services.Contracts
{
    public interface IMaintenanceService
    {
        void Run();
    }
}