﻿using FlightController.Domain.Model.Contracts;


namespace FlightController.Domain.Services.Contracts
{
    public interface IMaintenanceConditionService
        : IFlyingObjectVisitor
    {
        bool IsMaintenanceNeeded { get; }
    }
}