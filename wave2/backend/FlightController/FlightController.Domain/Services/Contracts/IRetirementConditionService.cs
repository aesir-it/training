﻿using FlightController.Domain.Model.Contracts;


namespace FlightController.Domain.Services.Contracts
{
    public interface IRetirementConditionService
        : IFlyingObjectVisitor
    {
        bool IsRetirementNeeded { get; }
    }
}