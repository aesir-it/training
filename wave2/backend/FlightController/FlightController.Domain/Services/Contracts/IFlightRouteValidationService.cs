﻿using FlightController.Domain.Model;


namespace FlightController.Domain.Services.Contracts
{
    public interface IFlightRouteValidationService
    {
        void Validate(Airport departure, Airport destination);
    }
}