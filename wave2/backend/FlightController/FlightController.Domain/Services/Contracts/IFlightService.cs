﻿namespace FlightController.Domain.Services.Contracts
{
    public interface IFlightService
    {
        void Run();
    }
}