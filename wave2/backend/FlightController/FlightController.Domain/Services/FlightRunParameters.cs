﻿namespace FlightController.Domain.Services
{
    public class FlightRunParameters
    {
        public int RetirementThreshold { get; private set; }



        public FlightRunParameters(int retirementThreshold)
        {
            RetirementThreshold = retirementThreshold;
        }
    }
}