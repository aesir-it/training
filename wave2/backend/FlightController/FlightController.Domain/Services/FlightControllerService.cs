﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightController.Domain.Calculators.Contracts;
using FlightController.Domain.Model;
using FlightController.Domain.Repositories.Contracts;
using FlightController.Domain.Services.Contracts;


namespace FlightController.Domain.Services
{
    public class FlightControllerService
        : IFlightControllerService
    {
        private readonly IDurationCalculator _durationCalculator;
        private readonly IFlightService _flightService;
        private readonly IMaintenanceService _maintenanceService;
        private readonly IFlightControllerRepository _flightControllerRepository;



        public FlightControllerService(IDurationCalculator durationCalculator, IFlightService flightService, IMaintenanceService maintenanceService, IFlightControllerRepository flightControllerRepository)
        {
            _durationCalculator = durationCalculator;
            _flightService = flightService;
            _maintenanceService = maintenanceService;
            _flightControllerRepository = flightControllerRepository;
        }


        public FlightRunResult Run(FlightRunParameters flightRunParameters)
        {
            //var randomizer = new Random(Environment.TickCount);

            while (_flightControllerRepository.HasActiveFlyingObjects())
            {
                //foreach (var flyingObject in activeFlyingObjects.ToList())
                //{
                    //var potentialFlightRoutes =
                    //    _flightRoutes
                    //        .Where(x => x.Departure == flyingObject.CurrentAirport)
                    //        .ToList();

                    //if (potentialFlightRoutes.Count == 0)
                    //{
                    //    activeFlyingObjects.Remove(flyingObject);

                    //    continue;
                    //}

                    //var selectedIndex = randomizer.Next(0, potentialFlightRoutes.Count);

                    //var flightRoute = potentialFlightRoutes[selectedIndex];

#warning TODO: move to retirement service or something.
                    //if (flyingObject.Status == FlyingObjectStatus.Retired)
                    //{
                    //    activeFlyingObjects.Remove(flyingObject);
                    //}
                //}
                _flightService.Run();
                _maintenanceService.Run();
            }

            var delta = _durationCalculator.GetSample();

            var flyingObjects =
                _flightControllerRepository
                    .GetAllFlyingObjects()
                    .ToList();

            return new FlightRunResult(flyingObjects, delta);
        }
    }
}