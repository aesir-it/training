﻿using FlightController.Domain.Calculators.Contracts;
using FlightController.Domain.Repositories.Contracts;
using FlightController.Domain.Services.Contracts;


namespace FlightController.Domain.Services
{
    public class FlightService
        : IFlightService
    {
        private readonly IFlightControllerRepository _flightControllerRepository;
        private readonly IFlightRouteCalculator _flightRouteCalculator;
        private readonly IMaintenanceConditionService _maintenanceConditionService;
        private readonly IRetirementConditionService _retirementConditionService;



        public FlightService(IFlightControllerRepository flightControllerRepository, IFlightRouteCalculator flightRouteCalculator, IMaintenanceConditionService maintenanceConditionService, IRetirementConditionService retirementConditionService)
        {
            _flightControllerRepository = flightControllerRepository;
            _flightRouteCalculator = flightRouteCalculator;
            _maintenanceConditionService = maintenanceConditionService;
            _retirementConditionService = retirementConditionService;
        }


        public void Run()
        {
            var flyingObjects = _flightControllerRepository.GetFlyingObjectsOnTheGround();

            foreach (var flyingObject in flyingObjects)
            {
                var suitableFlightRoute = _flightRouteCalculator.CalculateSuitableFlightRoute(flyingObject);

                if (suitableFlightRoute == null)
                {
                    _flightControllerRepository.RemoveDueToInvalidFlightRoute(flyingObject);

                    continue;
                }

                flyingObject.TakeOff(suitableFlightRoute);
                flyingObject.Land(_maintenanceConditionService, _retirementConditionService);
            }
        }
    }
}