﻿using FlightController.Domain.Model;


namespace FlightController.Domain.Calculators.Contracts
{
    public interface IFlightRouteCalculator
    {
        FlightRoute CalculateSuitableFlightRoute(FlyingObject flyingObject);
    }
}