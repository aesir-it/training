﻿using System;


namespace FlightController.Domain.Calculators.Contracts
{
    public interface IDurationCalculator
    {
        TimeSpan GetSample();
    }
}