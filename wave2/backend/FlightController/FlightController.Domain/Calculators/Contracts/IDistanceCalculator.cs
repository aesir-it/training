﻿using FlightController.Domain.Model;


namespace FlightController.Domain.Calculators.Contracts
{
    public interface IDistanceCalculator
    {
        double Calculate(Airport departure, Airport destination);
    }
}