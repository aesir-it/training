﻿using FlightController.Domain.Calculators.Contracts;
using FlightController.Domain.Model;


namespace FlightController.Domain.Calculators
{
    public class CrappyNonTestedDistanceCalculator
        : IDistanceCalculator
    {
        public double Calculate(Airport departure, Airport destination)
        {
            return departure.Code == "BRU" ? 1000d : 5000d;
        }
    }
}