﻿using FlightController.Domain.Calculators;
using NUnit.Framework;
using TestSupport;


// ReSharper disable once CheckNamespace
namespace FlightController.Domain.Test.Calculators.DateTimeDurationCalculatorTest
{
    [TestFixture]
    public class Given_Valid_Dependencies_When_Constructing_Instance
        : ArrangeActAssertTest
    {
        private DateTimeDurationCalculator _sut;



        protected override void Arrange()
        {
        }


        protected override void Act()
        {
            _sut = new DateTimeDurationCalculator();
        }


        [Test]
        public void Then_It_Should_Create_A_Valid_Instance()
        {
            Assert.IsNotNull(_sut);
        }
    }
}