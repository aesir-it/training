﻿using System.Collections.Generic;
using System.Linq;
using FlightController.Domain.Calculators.Contracts;
using FlightController.Domain.Model;
using FlightController.Domain.Services;
using FlightController.Domain.Services.Contracts;
using Moq;
using NUnit.Framework;
using TestSupport;


// ReSharper disable once CheckNamespace
namespace FlightController.Domain.Test.Model.FlyingObjectTest
{
    [TestFixture]
    public class Given_Valid_Dependencies_When_Constructing_Instance_With_Mock_Library
         : ArrangeActAssertTest
    {
        private FlyingObject _sut;
        private int _id;
        private string _code;
        private Airport _currentAirport;



        protected override void Arrange()
        {
            _id = 666;
            _code = "123-ABC";
            _currentAirport = new Airport("BRU");
        }


        protected override void Act()
        {
            var mock = new Mock<FlyingObject>(_id, _code, _currentAirport);
            _sut = mock.Object;
        }


        [Test]
        public void Then_It_Should_Create_A_Valid_Instance()
        {
            Assert.IsNotNull(_sut);
        }


        [Test]
        public void Then_It_Should_Have_Correct_Id()
        {
            Assert.AreEqual(_id, _sut.Id);
        }


        [Test]
        public void Then_It_Should_Have_Correct_Code()
        {
            Assert.AreEqual(_code, _sut.Code);
        }


        [Test]
        public void Then_It_Should_Have_Correct_Current_Airport()
        {
            Assert.AreEqual(_currentAirport, _sut.CurrentAirport);
        }


        [Test]
        public void Then_It_Should_Have_Correct_Status()
        {
            Assert.AreEqual(FlyingObjectStatus.OnTheGround, _sut.Status);
        }
    }



    [TestFixture]
    public abstract class Given_Valid_Dependencies_And_Constructed_Instance_Scenario
        : ArrangeActAssertTest
    {
        protected FlyingObject _sut;
        protected Mock<FlyingObject> _mock;
        protected IList<FlyingObjectStatus> _statusList;



        protected override void Arrange()
        {
            const int id = 666;
            const string code = "123-ABC";
            var currentAirport = new Airport("BRU");

            _mock =
                new Mock<FlyingObject>(id, code, currentAirport)
                {
                    CallBase = true
                };
            _sut = _mock.Object;

            _statusList = new List<FlyingObjectStatus>();

            _sut.StatusChanged += (sender, args) => _statusList.Add(args.Status);
        }
    }



    [TestFixture]
    public class Given_FlightRoute_When_Taking_Off
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private FlightRoute _flightRoute;



        protected override void Arrange()
        {
            base.Arrange();

            var departure = new Airport("a");
            var destination = new Airport("b");
            var flightRouteValidationService = new DifferentAirportFlightRouteValidationService();
            var distanceCalculator = Mock.Of<IDistanceCalculator>();

            _flightRoute = FlightRoute.Create(departure, destination, flightRouteValidationService, distanceCalculator);
        }


        protected override void Act()
        {
            _sut.TakeOff(_flightRoute);
        }


        [Test]
        public void Then_It_Should_Have_Null_Airport_As_Current_Airport()
        {
            Assert.AreEqual(Airport.Null, _sut.CurrentAirport);
        }


        [Test]
        public void Then_It_Should_Have_Incremented_The_TotalFlights()
        {
            Assert.AreEqual(1, _sut.TotalFlights);
        }


        [Test]
        public void Then_It_Should_Have_Correct_Status()
        {
            Assert.AreEqual(FlyingObjectStatus.Flying, _sut.Status);
        }


        [Test]
        public void Then_It_Should_Have_Raised_Correct_Number_Of_Events()
        {
            Assert.AreEqual(1, _statusList.Count);
        }


        [Test]
        public void Then_It_Should_Have_Raised_Flying_Status_As_First_Event()
        {
            Assert.AreEqual(FlyingObjectStatus.Flying, _statusList.First());
        }
    }



    [TestFixture]
    public abstract class Given_Flying_When_Landing_Scenario
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private FlightRoute _flightRoute;
        private Airport _destination;
        private Mock<IMaintenanceConditionService> _maintenanceConditionServiceMock;
        private IMaintenanceConditionService _maintenanceConditionService;
        private Mock<IRetirementConditionService> _retirementConditionServiceMock;
        private IRetirementConditionService _retirementConditionService;



        protected abstract bool IsMaintenanceNeeded { get; }


        protected abstract bool IsRetirementNeeded { get; }


        protected abstract FlyingObjectStatus ExpectedStatus { get; }


        protected abstract int ExpectedNumberOfEvents { get; }



        protected override void Arrange()
        {
            base.Arrange();

            var departure = new Airport("a");
            _destination = new Airport("b");
            var flightRouteValidationService = new DifferentAirportFlightRouteValidationService();

            var distanceCalculatorMock = new Mock<IDistanceCalculator>();
            var distanceCalculator = distanceCalculatorMock.Object;

            distanceCalculatorMock
                .Setup(x => x.Calculate(departure, _destination))
                .Returns(123d);

            _maintenanceConditionServiceMock = new Mock<IMaintenanceConditionService>();
            _maintenanceConditionService = _maintenanceConditionServiceMock.Object;

            _maintenanceConditionServiceMock
                .Setup(x => x.IsMaintenanceNeeded)
                .Returns(IsMaintenanceNeeded);

            _retirementConditionServiceMock = new Mock<IRetirementConditionService>();
            _retirementConditionService = _retirementConditionServiceMock.Object;

            _retirementConditionServiceMock
                .Setup(x => x.IsRetirementNeeded)
                .Returns(IsRetirementNeeded);

            _flightRoute = FlightRoute.Create(departure, _destination, flightRouteValidationService, distanceCalculator);

            _sut.TakeOff(_flightRoute);
        }


        protected override void Act()
        {
            _sut.Land(_maintenanceConditionService, _retirementConditionService);
        }


        [Test]
        public void Then_It_Should_Have_Destination_Airport_As_Current_Airport()
        {
            Assert.AreEqual(_destination, _sut.CurrentAirport);
        }


        [Test]
        public void Then_It_Should_Have_Incremented_AirMiles()
        {
            Assert.AreEqual(_flightRoute.Distance, _sut.AirMiles);
        }


        [Test]
        public void Then_It_Should_Not_Increment_MaintenanceCount()
        {
            Assert.AreEqual(0, _sut.MaintenanceCount);
        }


        [Test]
        public void Then_It_Should_Visit_MaintenanceConditionService()
        {
            _mock.Verify(x => x.Visit(_maintenanceConditionService), Times.Once);
            //_flyingObjectVisitorMock.Verify(x => x.Visit(_sut), Times.Once);
        }


        [Test]
        public void Then_It_Should_Visit_RetirementConditionService()
        {
            _mock.Verify(x => x.Visit(_retirementConditionService), Times.Once);
            //_flyingObjectVisitorMock.Verify(x => x.Visit(_sut), Times.Once);
        }


        [Test]
        public void Then_It_Should_Have_Correct_Status()
        {
            Assert.AreEqual(ExpectedStatus, _sut.Status);
        }


        [Test]
        public void Then_It_Should_Have_Raised_Correct_Number_Of_Events()
        {
            Assert.AreEqual(ExpectedNumberOfEvents, _statusList.Count);
        }


        [Test]
        public void Then_It_Should_Have_Raised_Flying_Status_As_First_Event()
        {
            Assert.AreEqual(FlyingObjectStatus.Flying, _statusList.First());
        }


        [Test]
        public void Then_It_Should_Have_Raised_OnTheGround_Status_As_Second_Event()
        {
            Assert.AreEqual(FlyingObjectStatus.OnTheGround, _statusList[1]);
        }
    }



    [TestFixture]
    public class Given_No_Maintenance_Needed_And_Not_Retiring
        : Given_Flying_When_Landing_Scenario
    {
        protected override bool IsMaintenanceNeeded => false;


        protected override bool IsRetirementNeeded => false;


        protected override FlyingObjectStatus ExpectedStatus => FlyingObjectStatus.OnTheGround;


        protected override int ExpectedNumberOfEvents => 2;
    }



    [TestFixture]
    public class Given_Flying_And_Maintenance_Needed_And_Not_Retiring_When_Landing
        : Given_Flying_When_Landing_Scenario
    {
        protected override bool IsMaintenanceNeeded => true;


        protected override bool IsRetirementNeeded => false;


        protected override FlyingObjectStatus ExpectedStatus => FlyingObjectStatus.MaintenanceNeeded;


        protected override int ExpectedNumberOfEvents => 3;



        [Test]
        public void Then_It_Should_Have_Raised_MaintenanceNeeded_Status_As_Third_Event()
        {
            Assert.AreEqual(FlyingObjectStatus.MaintenanceNeeded, _statusList[2]);
        }
    }



    [TestFixture]
    public class Given_Flying_And_No_Maintenance_Needed_And_Retiring_When_Landing
        : Given_Flying_When_Landing_Scenario
    {
        protected override bool IsMaintenanceNeeded => false;


        protected override bool IsRetirementNeeded => true;


        protected override FlyingObjectStatus ExpectedStatus => FlyingObjectStatus.Retired;


        protected override int ExpectedNumberOfEvents => 3;



        [Test]
        public void Then_It_Should_Have_Raised_Retired_Status_As_Third_Event()
        {
            Assert.AreEqual(FlyingObjectStatus.Retired, _statusList[2]);
        }
    }



    [TestFixture]
    public class Given_Flying_And_Maintenance_Needed_And_Retiring_When_Landing
        : Given_Flying_When_Landing_Scenario
    {
        protected override bool IsMaintenanceNeeded => true;


        protected override bool IsRetirementNeeded => true;


        protected override FlyingObjectStatus ExpectedStatus => FlyingObjectStatus.Retired;


        protected override int ExpectedNumberOfEvents => 4;



        [Test]
        public void Then_It_Should_Have_Raised_MaintenanceNeeded_Status_As_Third_Event()
        {
            Assert.AreEqual(FlyingObjectStatus.MaintenanceNeeded, _statusList[2]);
        }


        [Test]
        public void Then_It_Should_Have_Raised_Retired_Status_As_Fourth_Event()
        {
            Assert.AreEqual(FlyingObjectStatus.Retired, _statusList[3]);
        }
    }



    [TestFixture]
    public class Given_When_Performing_Maintenance
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        protected override void Act()
        {
            _sut.PerformMaintenance();
        }


        [Test]
        public void Then_It_Should_Have_OnTheGround_As_Status()
        {
            Assert.AreEqual(FlyingObjectStatus.OnTheGround, _sut.Status);
        }


        [Test]
        public void Then_It_Should_Have_Raised_Only_1_Event()
        {
            Assert.AreEqual(1, _statusList.Count);
        }


        [Test]
        public void Then_It_Should_Have_Raised_OnTheGround_StatusChanged_Event()
        {
            Assert.AreEqual(FlyingObjectStatus.OnTheGround, _statusList[0]);
        }
    }
}