﻿using System.Collections.Generic;
using System.Linq;
using FlightController.Domain.Model;
using Moq;


namespace FlightController.Domain.Test.Model.TestSupport
{
    public static class FlyingObjectFactory
    {
        public static IList<Mock<FlyingObject>> CreateMockList()
        {
            var airport = new Airport("BRU", "Brussels Airport");

            var flyingObjectMocks =
                Enumerable
                    .Range(0, 3)
                    .Select(id =>
                        new Mock<FlyingObject>(id, $"{id}", airport)
                        {
                            CallBase = true
                        })
                    .ToList();

            return flyingObjectMocks;
        }
    }
}