﻿using FlightController.Domain.Model;
using NUnit.Framework;
using TestSupport;


// ReSharper disable once CheckNamespace
namespace FlightController.Domain.Test.Model.AirplaneTest
{
    [TestFixture]
    public class Given_Valid_Dependencies_When_Constructing_Instance
        : ArrangeActAssertTest
    {
        private Airplane _sut;



        protected override void Arrange()
        {
        }


        protected override void Act()
        {
            var currentAirport = new Airport("code");

            _sut = new Airplane(1, "Airforce 1", currentAirport);
        }


        [Test]
        public void Then_It_Should_Create_A_Valid_Instance()
        {
            Assert.IsNotNull(_sut);
        }
    }



    [TestFixture]
    public abstract class Given_Valid_Dependencies_And_Constructed_Instance_Scenario
        : ArrangeActAssertTest
    {
        private Airplane _sut;



        protected override void Arrange()
        {
            var currentAirport = new Airport("code");

            _sut = new Airplane(1, "Airforce 1", currentAirport);
        }
    }
}