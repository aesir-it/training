﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using FlightController.Domain.Model;
using FlightController.Domain.Model.EventArguments;
using FlightController.Domain.Repositories.Contracts;
using FlightController.Domain.Services;
using FlightController.Domain.Test.Model.TestSupport;
using Moq;
using NUnit.Framework;
using TestSupport;


// ReSharper disable once CheckNamespace
namespace FlightController.Domain.Test.Services.MaintenanceServiceTest
{
    [TestFixture]
    public class Given_Valid_Dependencies_When_Constructing_Instance
        : ArrangeActAssertTest
    {
        private MaintenanceService _sut;
        private IFlightControllerRepository _flightControllerRepository;



        protected override void Arrange()
        {
            _flightControllerRepository = Mock.Of<IFlightControllerRepository>();
        }


        protected override void Act()
        {
            _sut = new MaintenanceService(_flightControllerRepository);
        }


        [Test]
        public void Then_It_Should_Create_A_Valid_Instance()
        {
            Assert.IsNotNull(_sut);
        }
    }



    [TestFixture]
    public abstract class Given_Valid_Dependencies_And_Constructed_Instance_Scenario
        : ArrangeActAssertTest
    {
        protected MaintenanceService _sut;
        protected Mock<IFlightControllerRepository> _flightControllerRepositoryMock;



        protected override void Arrange()
        {
            _flightControllerRepositoryMock = new Mock<IFlightControllerRepository>();
            var flightControllerRepository = _flightControllerRepositoryMock.Object;

            _sut = new MaintenanceService(flightControllerRepository);
        }
    }



    [TestFixture]
    public class Given_When_Running
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private IList<Mock<FlyingObject>> _flyingObjectMocks;
        private IList<FlyingObject> _flyingObjects;
        private IList<StatusChangedEventArgs> _statusChangedList;



        protected override void Arrange()
        {
            base.Arrange();

            _statusChangedList = new List<StatusChangedEventArgs>();

            _flyingObjectMocks = FlyingObjectFactory.CreateMockList();
            _flyingObjects = _flyingObjectMocks.Select(x => x.Object).ToList();

            _flyingObjects.ForEach(x => x.StatusChanged += (sender, args) => _statusChangedList.Add(args));

            _flightControllerRepositoryMock
                .Setup(x => x.GetFlyingObjectNeedingMaintenance())
                .Returns(_flyingObjects);
        }


        protected override void Act()
        {
            _sut.Run();
        }


        [Test]
        public void Then_It_Should_Have_Retrieved_FlyingObjects_Needing_Maintenance_From_FlightControllerRepository()
        {
            _flightControllerRepositoryMock.Verify(x => x.GetFlyingObjectNeedingMaintenance(), Times.Once);
        }


        // This test is impossible due to the PerformMaintenance method not being overridable by the mocking framework.
        //[Test]
        //public void Then_It_Should_Have_Performed_Maintenance_On_FlyingObjects()
        //{
        //    _flyingObjectMocks
        //        .ToList()
        //        .ForEach(mock => mock.Verify(flyingObject => flyingObject.PerformMaintenance(), Times.Once));
        //}


        [Test]
        public void Then_It_Should_Have_Incremented_Maintenance_Count()
        {
            _flyingObjects
                .ToList()
                .ForEach(x => Assert.AreEqual(1, x.MaintenanceCount));
        }


        [Test]
        public void Then_It_Should_Have_OnTheGround_As_Status_For_All_FlyingObjects()
        {
            _flyingObjects
                .ToList()
                .ForEach(x => Assert.AreEqual(FlyingObjectStatus.OnTheGround, x.Status));
        }


        [Test]
        public void Then_It_Should_Have_Raised_OnTheGround_StatusChanged_Event_For_All_FlyingObjects()
        {
            Predicate<FlyingObject> checkIfOnTheGroundStatusChangedEventWasRaisedForFlyingObject =
                flyingObject =>
                {
                    // Not needed since Single will throw when no EventArgs is found for the flying object.
                    //if (_statusChangedList.Any(x => x.FlyingObject == flyingObject))
                    //{
                    //    return false;
                    //}

                    var statusChangedEventArgs = _statusChangedList.Single(x => x.FlyingObject == flyingObject);

                    return statusChangedEventArgs.Status == FlyingObjectStatus.OnTheGround;
                };

            _flyingObjects
                .ToList()
                .ForEach(flyingObject => Assert.IsTrue(checkIfOnTheGroundStatusChangedEventWasRaisedForFlyingObject(flyingObject)));
        }
    }
}