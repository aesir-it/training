﻿using System;
using FlightController.Domain.Model;
using FlightController.Domain.Services;
using NUnit.Framework;
using TestSupport;


// ReSharper disable once CheckNamespace
namespace FlightController.Domain.Test.Services.DifferentAirportFlightRouteValidationServiceTest
{
    [TestFixture]
    public class Given_Valid_Dependencies_When_Constructing_Instance
        : ArrangeActAssertTest
    {
        private DifferentAirportFlightRouteValidationService _sut;



        protected override void Arrange()
        {
        }


        protected override void Act()
        {
            _sut = new DifferentAirportFlightRouteValidationService();
        }


        [Test]
        public void Then_It_Should_Create_A_Valid_Instance()
        {
            Assert.IsNotNull(_sut);
        }
    }



    [TestFixture]
    public abstract class Given_Valid_Dependencies_And_Constructed_Instance_Scenario
        : ArrangeActAssertTest
    {
        protected DifferentAirportFlightRouteValidationService _sut;



        protected override void Arrange()
        {
            _sut = new DifferentAirportFlightRouteValidationService();
        }
    }



    [TestFixture]
    public class Given_Departure_And_Destination_Are_The_Same_When_Validating
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private Airport _departure;
        private Airport _destination;
        private ExceptionCatcher _result;



        protected override void Arrange()
        {
            base.Arrange();

            const string code = "Code";

            _departure = new Airport(code);
            _destination = new Airport(code);
        }


        protected override void Act()
        {
            Action action = () => _sut.Validate(_departure, _destination);
            _result = action.ExecuteWithCatch();
        }


        [Test]
        public void Then_It_Should_Thrown_An_Exception()
        {
            Assert.IsInstanceOf<Exception>(_result.Exception);
        }
    }



    [TestFixture]
    public class Given_Departure_And_Destination_Are_Not_The_Same_When_Validating
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private Airport _departure;
        private Airport _destination;
        private ExceptionCatcher _result;



        protected override void Arrange()
        {
            base.Arrange();

            _departure = new Airport("Code1");
            _destination = new Airport("Code2");
        }


        protected override void Act()
        {
            Action action = () => _sut.Validate(_departure, _destination);
            _result = action.ExecuteWithCatch();
        }


        [Test]
        public void Then_It_Should_Not_Thrown_An_Exception()
        {
            Assert.IsNull(_result.Exception);
        }
    }
}