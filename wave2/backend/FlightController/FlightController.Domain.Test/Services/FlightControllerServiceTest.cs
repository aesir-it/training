﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightController.Domain.Calculators.Contracts;
using FlightController.Domain.Model;
using FlightController.Domain.Repositories.Contracts;
using FlightController.Domain.Services;
using FlightController.Domain.Services.Contracts;
using Moq;
using NUnit.Framework;
using TestSupport;


// ReSharper disable once CheckNamespace
namespace FlightController.Domain.Test.Services.FlightControllerServiceTest
{
    [TestFixture]
    public class Given_Valid_Dependencies_When_Constructing_Instance
        : ArrangeActAssertTest
    {
        private FlightControllerService _sut;
        private IDurationCalculator _durationCalculator;
        private IFlightService _flightService;
        private IMaintenanceService _maintenanceService;
        private IFlightControllerRepository _flightControllerRepository;



        protected override void Arrange()
        {
            _durationCalculator = Mock.Of<IDurationCalculator>();
            _flightService = Mock.Of<IFlightService>();
            _maintenanceService = Mock.Of<IMaintenanceService>();
            _flightControllerRepository = Mock.Of<IFlightControllerRepository>();
        }


        protected override void Act()
        {
            _sut = new FlightControllerService(_durationCalculator, _flightService, _maintenanceService, _flightControllerRepository);
        }


        [Test]
        public void Then_It_Should_Create_A_Valid_Instance()
        {
            Assert.IsNotNull(_sut);
        }
    }



    [TestFixture]
    public abstract class Given_Valid_Dependencies_And_Constructed_Instance_Scenario
        : ArrangeActAssertTest
    {
        protected FlightControllerService _sut;
        protected Mock<IDurationCalculator> _durationCalculatorMock;
        protected List<FlyingObject> _flyingObjects;
        protected Mock<IFlightService> _flightServiceMock;
        protected Mock<IMaintenanceService> _maintenanceServiceMock;
        protected Mock<IFlightControllerRepository> _flightControllerRepositoryMock;



        protected override void Arrange()
        {
            _flyingObjects = CreateFlyingObjects();

            _durationCalculatorMock = new Mock<IDurationCalculator>();
            var durationCalculator = _durationCalculatorMock.Object;

            _flightServiceMock = new Mock<IFlightService>();
            var flightService = _flightServiceMock.Object;

            _maintenanceServiceMock = new Mock<IMaintenanceService>();
            var maintenanceService = _maintenanceServiceMock.Object;

            _flightControllerRepositoryMock = new Mock<IFlightControllerRepository>();
            var flightControllerRepository = _flightControllerRepositoryMock.Object;

            _flightControllerRepositoryMock
                .Setup(x => x.HasActiveFlyingObjects())
                .Returns(HasActiveFlyingObjects);

            _flightControllerRepositoryMock
                .Setup(x => x.GetAllFlyingObjects())
                .Returns(_flyingObjects);

            _sut = new FlightControllerService(durationCalculator, flightService, maintenanceService, flightControllerRepository);
        }


        protected abstract List<FlyingObject> CreateFlyingObjects();


        protected abstract bool HasActiveFlyingObjects();


        //protected abstract List<FlightRoute> CreateFlightRoutes();
    }



    [TestFixture]
    public abstract class Given_FlyingObjects_And_FlightRoutes_When_Running_Scenario
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private readonly TimeSpan _expectedTotalDuration = TimeSpan.FromMilliseconds(123d);



        protected FuncExtensions.FuncResult<FlightRunResult> _executionResult;
        private FlightRunParameters _flightRunParameters;


        protected override void Arrange()
        {
            base.Arrange();

            _durationCalculatorMock
                .Setup(x => x.GetSample())
                .Returns(_expectedTotalDuration);

            _flightRunParameters = new FlightRunParameters(666);
        }


        protected override void Act()
        {
            Func<FlightRunResult> func = () =>_sut.Run(_flightRunParameters);
            _executionResult = func.ExecuteWithCatch();
        }


        [Test]
        public void Then_It_Should_Return_Valid_Result()
        {
            Assert.IsNotNull(_executionResult.Result);
        }


        [Test]
        public void Then_It_Should_Have_Correct_TotalDuration()
        {
            Assert.AreEqual(_expectedTotalDuration.TotalMilliseconds, _executionResult.Result.TotalDuration);
        }


        [Test]
        public void Then_It_Should_Calculate_Total_Duration_Using_DurationCalculator()
        {
            _durationCalculatorMock.Verify(x => x.GetSample(), Times.Once);
        }


        [Test]
        public void Then_It_Should_Check_If_It_Has_Active_FlyingObjects_Using_FlightControllerRepository()
        {
            _flightControllerRepositoryMock.Verify(x => x.HasActiveFlyingObjects(), Times.Exactly(_flyingObjects.Count + 1));
        }


        [Test]
        public void Then_It_Should_Get_All_FlyingObjects_For_Result_Using_FlightControllerRepository()
        {
            _flightControllerRepositoryMock.Verify(x => x.GetAllFlyingObjects(), Times.Once);
        }


        [Test]
        public void Then_It_Should_Put_All_FlyingObjects_From_FlightControllerRepository_In_Result()
        {
            CollectionAssert.AreEqual(_flyingObjects, _executionResult.Result.FlyingObjects);
            //Assert.AreSame(_flyingObjects, _executionResult.Result.FlyingObjects);
        }
    }



    [TestFixture]
    public class Given_No_FlyingObjects_When_Running
        : Given_FlyingObjects_And_FlightRoutes_When_Running_Scenario
    {
        protected override List<FlyingObject> CreateFlyingObjects() => new List<FlyingObject>();



        protected override bool HasActiveFlyingObjects()
        {
            return false;
        }


        [Test]
        public void Then_It_Should_Not_Run_FlightService()
        {
            _flightServiceMock.Verify(x => x.Run(), Times.Never);
        }


        [Test]
        public void Then_It_Should_Not_Run_MaintenanceService()
        {
            _maintenanceServiceMock.Verify(x => x.Run(), Times.Never);
        }
    }



    [TestFixture]
    public class Given_FlyingObjects_When_Running
        : Given_FlyingObjects_And_FlightRoutes_When_Running_Scenario
    {
        private int _runCounter;



        protected override void Arrange()
        {
            base.Arrange();

            _runCounter = 0;
        }


        protected override List<FlyingObject> CreateFlyingObjects() =>
            new List<FlyingObject>
                {
                    new Airplane(1, "Airforce 1", new Airport("BRU"))
                };


        protected override bool HasActiveFlyingObjects()
        {
            _runCounter++;

            var isSecondRun = _runCounter != 2;

            return isSecondRun;
        }


        [Test]
        public void Then_It_Should_Run_FlightService()
        {
            _flightServiceMock.Verify(x => x.Run(), Times.Once);
        }


        [Test]
        public void Then_It_Should_Run_MaintenanceService()
        {
            _maintenanceServiceMock.Verify(x => x.Run(), Times.Once);
        }
    }



    //[TestFixture]
    //public abstract class Given_FlyingObjects_And_FlightRoutes_When_Running_Scenario
    //    : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    //{
    //    private readonly TimeSpan _expectedTotalDuration = TimeSpan.FromMilliseconds(123d);



    //    protected FuncExtensions.FuncResult<FlightRunResult> _executionResult;
    //    private FlightRunParameters _flightRunParameters;


    //    protected abstract int ExpectedTotalFlightCount { get; }


    //    protected abstract int MaintenanceCount { get; }



    //    protected override void Arrange()
    //    {
    //        base.Arrange();

    //        _durationCalculatorMock
    //            .Setup(x => x.GetSample())
    //            .Returns(_expectedTotalDuration);

    //        _flightRunParameters = new FlightRunParameters(MaintenanceCount);
    //    }


    //    protected override void Act()
    //    {
    //        Func<FlightRunResult> func = () =>_sut.Run(_flightRunParameters);
    //        _executionResult = func.ExecuteWithCatch();
    //    }


    //    [Test]
    //    public void Then_It_Should_Return_Valid_Result()
    //    {
    //        Assert.IsNotNull(_executionResult.Result);
    //    }


    //    [Test]
    //    public void Then_It_Should_Have_Correct_TotalFlightCount()
    //    {
    //        Assert.AreEqual(ExpectedTotalFlightCount, _executionResult.Result.TotalFlightCount);
    //    }


    //    [Test]
    //    public void Then_It_Should_Have_Correct_TotalDuration()
    //    {
    //        Assert.AreEqual(_expectedTotalDuration.TotalMilliseconds, _executionResult.Result.TotalDuration);
    //    }


    //    [Test]
    //    public void Then_It_Calculate_Total_Duration_Using_DurationCalculator()
    //    {
    //        _durationCalculatorMock.Verify(x => x.GetSample(), Times.Once);
    //    }
    //}



    //[TestFixture]
    //public class Given_No_FlyingObjects_And_No_FlightRoutes_When_Running
    //    : Given_FlyingObjects_And_FlightRoutes_When_Running_Scenario
    //{
    //    protected override int ExpectedTotalFlightCount => 0;


    //    protected override int MaintenanceCount => 0;



    //    protected override List<FlyingObject> CreateFlyingObject() => new List<FlyingObject>();


    //    protected override List<FlightRoute> CreateFlightRoutes() => new List<FlightRoute>();
    //}



    //[TestFixture]
    //public class Given_One_FlyingObject_And_Three_FlightRoutes_And_After_Three_Iterations_No_More_FlightRoutes_Are_Available_When_Running
    //    : Given_FlyingObjects_And_FlightRoutes_When_Running_Scenario
    //{
    //    private IDistanceCalculator _distanceCalculator;


    //    protected override int ExpectedTotalFlightCount => 3;


    //    protected override int MaintenanceCount => 100;



    //    protected override void Arrange()
    //    {
    //        var distanceCalculatorMock = new Mock<IDistanceCalculator>();
    //        _distanceCalculator = distanceCalculatorMock.Object;

    //        distanceCalculatorMock
    //            .Setup(x => x.Calculate(new Airport("BRU", ""), new Airport("TFS", "")))
    //            .Returns(3000d);

    //        distanceCalculatorMock
    //            .Setup(x => x.Calculate(new Airport("TFS", ""), new Airport("ALC", "")))
    //            .Returns(1000d);

    //        distanceCalculatorMock
    //            .Setup(x => x.Calculate(new Airport("ALC", ""), new Airport("OST", "")))
    //            .Returns(2000d);

    //        base.Arrange();
    //    }


    //    protected override List<FlyingObject> CreateFlyingObject() =>
    //        new List<FlyingObject>
    //        {
    //            new Airplane(1, "Airforce 1", new Airport("BRU"))
    //        };


    //    protected override List<FlightRoute> CreateFlightRoutes()
    //    {
    //        var differentAirportFlightRouteValidationService = new DifferentAirportFlightRouteValidationService();

    //        return
    //            new List<FlightRoute>
    //            {
    //                FlightRoute.Create(new Airport("BRU"), new Airport("TFS"),
    //                    differentAirportFlightRouteValidationService, _distanceCalculator),
    //                FlightRoute.Create(new Airport("TFS"), new Airport("ALC"),
    //                    differentAirportFlightRouteValidationService, _distanceCalculator),
    //                FlightRoute.Create(new Airport("ALC"), new Airport("OST"),
    //                    differentAirportFlightRouteValidationService, _distanceCalculator)
    //            };
    //    }


    //    [Test]
    //    public void Then_It_Should_Not_Throw_An_Invalid_Operation_Exception()
    //    {
    //        Assert.IsNotInstanceOf<InvalidOperationException>(_executionResult.ExceptionCatcher.Exception);
    //    }
    //}



    //[TestFixture]
    //public class Given_One_FlyingObject_And_Two_FlightRoutes_That_Out_And_Return_And_Retirement_Needed_When_Running
    //    : Given_FlyingObjects_And_FlightRoutes_When_Running_Scenario
    //{
    //    private IDistanceCalculator _distanceCalculator;



    //    protected override int ExpectedTotalFlightCount => 1;


    //    protected override int MaintenanceCount => 0;



    //    protected override void Arrange()
    //    {
    //        var distanceCalculatorMock = new Mock<IDistanceCalculator>();
    //        _distanceCalculator = distanceCalculatorMock.Object;

    //        distanceCalculatorMock
    //            .Setup(x => x.Calculate(new Airport("BRU", ""), new Airport("TFS", "")))
    //            .Returns(3000d);

    //        distanceCalculatorMock
    //            .Setup(x => x.Calculate(new Airport("TFS", ""), new Airport("BRU", "")))
    //            .Returns(3000d);

    //        base.Arrange();

    //        _retirementConditionServiceMock
    //            .Setup(x => x.IsRetirementNeeded)
    //            .Returns(true);
    //    }


    //    protected override List<FlyingObject> CreateFlyingObject() =>
    //        new List<FlyingObject>
    //        {
    //            new Airplane(1, "Airforce 1", new Airport("BRU"))
    //        };


    //    protected override List<FlightRoute> CreateFlightRoutes()
    //    {
    //        var differentAirportFlightRouteValidationService = new DifferentAirportFlightRouteValidationService();

    //        return
    //            new List<FlightRoute>
    //            {
    //                FlightRoute.Create(new Airport("BRU"), new Airport("TFS"),
    //                    differentAirportFlightRouteValidationService, _distanceCalculator),
    //                FlightRoute.Create(new Airport("TFS"), new Airport("BRU"),
    //                    differentAirportFlightRouteValidationService, _distanceCalculator)
    //            };
    //    }


    //    [Test]
    //    public void Then_It_Should_Have_Correct_Maintenance_Count()
    //    {
    //        Assert.AreEqual(MaintenanceCount, _flyingObjects.First().MaintenanceCount);
    //    }
    //}



    //[TestFixture]
    //public class Given_One_FlyingObject_And_Two_FlightRoutes_That_Out_And_Return_And_Maintenance_Interval_When_Running
    //    : Given_FlyingObjects_And_FlightRoutes_When_Running_Scenario
    //{
    //    private IDistanceCalculator _distanceCalculator;
    //    private double _maintenanceInterval;



    //    protected override int ExpectedTotalFlightCount => 



    //    protected override void Arrange()
    //    {
    //        var distanceCalculatorMock = new Mock<IDistanceCalculator>();
    //        _distanceCalculator = distanceCalculatorMock.Object;

    //        distanceCalculatorMock
    //            .Setup(x => x.Calculate(new Airport("BRU", ""), new Airport("TFS", "")))
    //            .Returns(3000d);

    //        distanceCalculatorMock
    //            .Setup(x => x.Calculate(new Airport("TFS", ""), new Airport("BRU", "")))
    //            .Returns(3000d);

    //        _maintenanceInterval = 18000d;

    //        base.Arrange();
    //    }


    //    protected override List<FlyingObject> CreateFlyingObject() =>
    //        new List<FlyingObject>
    //        {
    //            new Airplane(1, "Airforce 1", new Airport("BRU"))
    //        };


    //    protected override List<FlightRoute> CreateFlightRoutes()
    //    {
    //        var differentAirportFlightRouteValidationService = new DifferentAirportFlightRouteValidationService();

    //        return
    //            new List<FlightRoute>
    //            {
    //                FlightRoute.Create(new Airport("BRU"), new Airport("TFS"),
    //                    differentAirportFlightRouteValidationService, _distanceCalculator),
    //                FlightRoute.Create(new Airport("TFS"), new Airport("BRU"),
    //                    differentAirportFlightRouteValidationService, _distanceCalculator)
    //            };
    //    }
    //}
}