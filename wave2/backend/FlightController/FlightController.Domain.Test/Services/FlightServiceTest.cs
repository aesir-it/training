﻿using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using FlightController.Domain.Calculators.Contracts;
using FlightController.Domain.Model;
using FlightController.Domain.Model.EventArguments;
using FlightController.Domain.Repositories.Contracts;
using FlightController.Domain.Services;
using FlightController.Domain.Services.Contracts;
using FlightController.Domain.Test.Model.TestSupport;
using Moq;
using NUnit.Framework;
using TestSupport;


// ReSharper disable once CheckNamespace
namespace FlightController.Domain.Test.Services.FlightServiceTest
{
    [TestFixture]
    public class Given_Valid_Dependencies_When_Constructing_Instance
        : ArrangeActAssertTest
    {
        private FlightService _sut;
        private IFlightControllerRepository _flightControllerRepository;
        private IFlightRouteCalculator _flightRouteCalculator;
        private IMaintenanceConditionService _maintenanceConditionService;
        private IRetirementConditionService _retirementConditionService;



        protected override void Arrange()
        {
            _flightControllerRepository = Mock.Of<IFlightControllerRepository>();
            _flightRouteCalculator = Mock.Of<IFlightRouteCalculator>();
            _maintenanceConditionService = Mock.Of<IMaintenanceConditionService>();
            _retirementConditionService = Mock.Of<IRetirementConditionService>();
        }


        protected override void Act()
        {
            _sut = new FlightService(_flightControllerRepository, _flightRouteCalculator, _maintenanceConditionService, _retirementConditionService);
        }


        [Test]
        public void Then_It_Should_Create_A_Valid_Instance()
        {
            Assert.IsNotNull(_sut);
        }
    }



    [TestFixture]
    public abstract class Given_Valid_Dependencies_And_Constructed_Instance_Scenario
        : ArrangeActAssertTest
    {
        protected FlightService _sut;
        protected Mock<IFlightControllerRepository> _flightControllerRepositoryMock;
        protected Mock<IFlightRouteCalculator> _flightRouteCalculatorMock;
        protected Mock<IMaintenanceConditionService> _maintenanceConditionServiceMock;
        protected Mock<IRetirementConditionService> _retirementConditionServiceMock;



        protected override void Arrange()
        {
            _flightControllerRepositoryMock = new Mock<IFlightControllerRepository>();
            var flightControllerRepository = _flightControllerRepositoryMock.Object;

            _flightRouteCalculatorMock = new Mock<IFlightRouteCalculator>();
            var flightRouteCalculator = _flightRouteCalculatorMock.Object;

            _maintenanceConditionServiceMock = new Mock<IMaintenanceConditionService>();
            var maintenanceConditionService = _maintenanceConditionServiceMock.Object;

            _retirementConditionServiceMock = new Mock<IRetirementConditionService>();
            var retirementConditionService = _retirementConditionServiceMock.Object;

            _sut = new FlightService(flightControllerRepository, flightRouteCalculator, maintenanceConditionService, retirementConditionService);
        }
    }



    [TestFixture]
    public abstract class Given_Flying_Objects_That_Are_On_The_Ground_When_Running_Scenario
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private IList<Mock<FlyingObject>> _flyingObjectMocks;
        protected IList<FlyingObject> _flyingObjects;
        protected IList<StatusChangedEventArgs> _statusList;



        protected override void Arrange()
        {
            base.Arrange();

            _flyingObjectMocks = FlyingObjectFactory.CreateMockList();
            _flyingObjects = _flyingObjectMocks.Select(x => x.Object).ToList();

            _statusList = new List<StatusChangedEventArgs>();
            _flyingObjects.ForEach(x => x.StatusChanged += (sender, args) => _statusList.Add(args));

            _flightControllerRepositoryMock
                .Setup(x => x.GetFlyingObjectsOnTheGround())
                .Returns(_flyingObjects);
        }


        protected override void Act()
        {
            _sut.Run();
        }


        [Test]
        public void Then_It_Should_Retrieve_FlyingObjects_That_Are_On_The_Ground_Using_FlightControllerRepository()
        {
            _flightControllerRepositoryMock.Verify(x => x.GetFlyingObjectsOnTheGround(), Times.Once);
        }


        [Test]
        public void Then_It_Should_Calculate_Suitable_FlightRoute_For_All_FlyingObjects()
        {
            _flyingObjects
                .ToList()
                .ForEach(flyingObject => _flightRouteCalculatorMock.Verify(y => y.CalculateSuitableFlightRoute(flyingObject), Times.Once));
        }
    }



    [TestFixture]
    public class Given_No_Suitable_FlightPlans_Available
        : Given_Flying_Objects_That_Are_On_The_Ground_When_Running_Scenario
    {
        protected override void Arrange()
        {
            base.Arrange();

            _flightRouteCalculatorMock
                .Setup(x => x.CalculateSuitableFlightRoute(It.IsAny<FlyingObject>()))
                .Returns<FlightRoute>(null);
        }


        [Test]
        public void Then_It_Should_Remove_FlyingObject_From_FlightControllerRepository()
        {
            _flyingObjects
                .ForEach(flyingObject => _flightControllerRepositoryMock.Verify(x => x.RemoveDueToInvalidFlightRoute(flyingObject), Times.Once));
        }
    }



    [TestFixture]
    public class Given_Suitable_FlightPlans_Available
        : Given_Flying_Objects_That_Are_On_The_Ground_When_Running_Scenario
    {
        protected override void Arrange()
        {
            base.Arrange();

            var flightRoute = FlightRoute.Create(new Airport("BRU", ""), new Airport("TFS", ""), Mock.Of<IFlightRouteValidationService>(), Mock.Of<IDistanceCalculator>());

            _flightRouteCalculatorMock
                .Setup(x => x.CalculateSuitableFlightRoute(It.IsAny<FlyingObject>()))
                .Returns(flightRoute);
        }


        [Test]
        public void Then_It_Should_Not_Remove_FlyingObject_From_FlightControllerRepository()
        {
            _flyingObjects
                .ForEach(flyingObject => _flightControllerRepositoryMock.Verify(x => x.RemoveDueToInvalidFlightRoute(flyingObject), Times.Never));
        }


        [Test]
        public void Then_It_Should_Have_Raised_Flying_Status_Changed_Event_For_All_FlyingObjects()
        {
            _flyingObjects
                .ForEach(flyingObject => Assert.IsTrue(_statusList.Any(x => x.Status == FlyingObjectStatus.Flying && x.FlyingObject == flyingObject)));
        }


        [Test]
        public void Then_It_Should_Have_Raised_OnTheGround_Status_Changed_Event_For_All_FlyingObjects()
        {
            _flyingObjects
                .ForEach(flyingObject => Assert.IsTrue(_statusList.Any(x => x.Status == FlyingObjectStatus.OnTheGround && x.FlyingObject == flyingObject)));
        }
    }
}