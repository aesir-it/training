﻿using System;

namespace LambdaFun
{
    class Program
    {
        public delegate void DwightAction();
        public delegate int DwightIntFunc();
        public delegate int DwightIntFuncWith2(int a, int b);
        public delegate TResult DwightFunc<out TResult>();


        static void Main(string[] args)
        {
            //DoSomething();

            Action action = DoSomething;
            //action();

            //action = DoSomething2;
            //action();

            //DwightAction dwiAction = DoSomething;
            //dwiAction();

            DoSomething3("Dwight");
 
            ExecuteSomeMethod(DoSomething);
            ExecuteSomeMethod(DoSomething2);


           //Action action2 = DoSomething3Dwight;
            Action action2 = () => DoSomething3("Dwight");

            ExecuteSomeMethod(action2);
            ExecuteSomeMethod(() => DoSomething3("Dwight"));

            action2 = () => DoSomething4("Dwight");
            ExecuteSomeMethod(action2);

            //ExecuteSomeMethod(DoSomething3, "Dwight");
            //ExecuteSomeMethod(DoSomething4, "Dwight");

            GiveMeSomething();

            Func<int> a = GiveMeSomething;

            DwightIntFunc dif = GiveMeSomething;

            DwightIntFuncWith2 dif2 = GiveMeSomething2;

            dif2(1, 2);


            Func<int, int, int> f = GiveMeSomething2;
            f(1, 2);

            Action aaa = GiveMeSomething3;
            ExecuteSomeMethod(aaa);

            aaa = () => GiveMeSomething();
            ExecuteSomeMethod(aaa);

            Console.ReadKey();
        }


        private static void GiveMeSomething3()
        {
            GiveMeSomething();
        }


        private static int GiveMeSomething()
        {
            return 666;
        }


        private static int GiveMeSomething2(int a, int b)
        {
            return a * b;
        }


        private static void DoSomething()
        {
            Console.WriteLine("DoSomething");
        }


        public static void DoSomething2()
        {
            Console.WriteLine("DoSomething2");
        }


        private static void DoSomething3(string message)
        {
            Console.WriteLine(message);
        }


        private static void DoSomething4(string message)
        {
            Console.WriteLine($"{DateTime.Now} - {message}");
        }


        //private static void DoSomething3Dwight()
        //{
        //    DoSomething3("Dwight");
        //}


        public static void ExecuteSomeMethod(Action action)
        {
            //Console.WriteLine("Executing");
            action();
        }


        public static void ExecuteSomeMethod(Action<string, int[], int[]> action, string message, int[] intArray)
        {
            Console.WriteLine("Executing");
            action(message, intArray, new int[3]);
        }


        public void InAction(string s, int[] ia, int[] ib)
        {
        }
    }
}