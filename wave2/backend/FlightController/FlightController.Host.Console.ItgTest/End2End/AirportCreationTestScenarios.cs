﻿using System.IO;
using System.Reflection;
using FlightController.Application.Model.Results;
using FlightController.Application.Services;
using FlightController.Domain.Model;
using FlightController.Domain.Repositories.Contracts;
using FlightController.Infra.EF.Repositories;
using FlightController.Infra.Text.Repositories;
using NUnit.Framework;
using TestSupport;


// ReSharper disable once CheckNamespace
namespace FlightController.Host.Console.ItgTest.End2End.AirportCreationTestScenarios
{
    [TestFixture]
    public abstract class Given_Valid_Dependencies_And_Constructed_Service_Instance_Scenario
        : ArrangeActAssertTest
    {
        protected AirportCreationService _sut;
        protected IAirportRepository _airportRepository;



        protected override void Arrange()
        {
            const string currentPath = "c:\\persist";

            _airportRepository = new TextFileBasedAirportRepository(currentPath);
            //_airportRepository = new AirportEFRepository();

            _sut = new AirportCreationService(_airportRepository);
        }
    }



    [TestFixture]
    public class Given_A_Name_And_Code_And_Airport_Does_Not_Exist_When_Creating
        : Given_Valid_Dependencies_And_Constructed_Service_Instance_Scenario
    {
        private string _code;
        private string _name;
        private AirportCreationResult _result;
        private Airport _airport;



        protected override void Arrange()
        {
            base.Arrange();

            _code = "BRU";
            _name = "Brussels Airport";
        }


        protected override void Act()
        {
            _result = _sut.Create(_code, _name);
            _airport = _airportRepository.Get(_code);
        }


        [Test]
        public void Then_It_Should_Return_Status_Ok()
        {
            Assert.AreEqual(ResultStatus.Ok, _result.Status);
        }


        [Test]
        public void Then_It_Should_Return_Correct_Code_In_Airport()
        {
            Assert.AreEqual(_code, _airport.Code);
        }


        [Test]
        public void Then_It_Should_Return_Correct_Name_In_Airport()
        {
            Assert.AreEqual(_name, _airport.Name);
        }
    }
}