﻿namespace FlightController.Host.WebApi.Models
{
    public class AirportDto
    {
        public string Code { get; set; }


        public string Name { get; set; }
    }
}