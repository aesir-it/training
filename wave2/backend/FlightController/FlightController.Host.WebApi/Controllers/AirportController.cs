﻿using System.Collections.Generic;
using System.Web.Http;
using FlightController.Host.WebApi.Models;

namespace FlightController.Host.WebApi.Controllers
{
    public class AirportController
        : ApiController
    {
        private string[] _list;


        public AirportController()
        {
            _list = new string[] { "value1", "value2" };
        }


        // GET: api/Airport
        public IEnumerable<string> Get()
        {
            return _list;
        }

        // GET: api/Airport/5
        public AirportDto Get(string code)
        {
            return
                new AirportDto
                {
                    Code = $"{code}",
                    Name = $"Name-{code}"
                };
        }

        //    // POST: api/Airport
        //    public void Post([FromBody]string value)
        //    {
        //    }

        //    // PUT: api/Airport/5
        //    public void Put(int id, [FromBody]string value)
        //    {
        //    }

        //    // DELETE: api/Airport/5
        //    public void Delete(int id)
        //    {
        //    }
    }
}