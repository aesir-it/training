﻿using System.IO;
using System.Text;
using FlightController.Domain.Model;
using FlightController.Domain.Repositories.Contracts;


namespace FlightController.Infra.Text.Repositories
{
    public class TextFileBasedAirportRepository
        : IAirportRepository
    {
        private readonly string _basePath;



        public TextFileBasedAirportRepository(string basePath)
        {
            _basePath = basePath;
        }


        public void Create(Airport airport)
        {
            var sb = new StringBuilder();
            sb.AppendLine(airport.Code);
            sb.AppendLine(airport.Name);

            var payLoad = sb.ToString();

            var filePath = CalculateFilePath(airport.Code);

            File.WriteAllText(filePath, payLoad);
        }


        public Airport Get(string code)
        {
            var filePath = CalculateFilePath(code);

            var payLoad = File.ReadAllLines(filePath);

            var airportCode = payLoad[0];
            var airportName = payLoad[1];

            return new Airport(airportCode, airportName);
        }


        private string CalculateFilePath(string airportCode)
        {
            var filePath = Path.Combine(_basePath, $"Repo_Airport_{airportCode}.txt");

            return filePath;
        }
    }
}