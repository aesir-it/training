﻿using FlightController.Application.Model.Results;
using FlightController.Application.Services;
using FlightController.Domain.Calculators;
using FlightController.Domain.Model;
using FlightController.Domain.Repositories.Contracts;
using FlightController.Domain.Services;
using FlightController.Infra.EF.Repositories;


namespace FlightController.Host.Console
{
    public class Program
    {
        static void Main()
        {
            IAirportRepository airportRepository = new AirportEFRepository();

            //var airportCreationService = new AirportCreationService(airportRepository);

            //AirportCreationResult result;

            //result = airportCreationService.Create("BRU", "Brussels Airport");
            //System.Console.WriteLine($"Result: {result.Status}");
            //result = airportCreationService.Create("OST", "Ostend Airport");
            //System.Console.WriteLine($"Result: {result.Status}");

            var brussels = airportRepository.Get("BRU");
            var ostend = airportRepository.Get("OST");

            IFlightRouteRepository flightRouteRepository = new FlightRouteRepository();


            //var bruOstflightRoute = FlightRoute.Create(brussels, ostend, new DifferentAirportFlightRouteValidationService(), new CrappyNonTestedDistanceCalculator());
            //flightRouteRepository.Create(bruOstflightRoute);


            var bruOstflightRouteCopy = flightRouteRepository.Get(brussels, ostend);

            System.Console.WriteLine($"Flight route from {bruOstflightRouteCopy.Departure} to {bruOstflightRouteCopy.Destination} is {bruOstflightRouteCopy.Distance} miles");



            System.Console.WriteLine("Press any key to quit...");
            System.Console.ReadKey(true);
        }
    }
}