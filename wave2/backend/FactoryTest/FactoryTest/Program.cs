﻿using System;

namespace FactoryTest
{
    class Program
    {
        static void Main(string[] args)
        {
            HandleCardMessage();
            HandleTextMessage();
        }


        private static void HandleTextMessage()
        {
            var message = MessageFactory.GetMessage<TextMessage>(MessageType.Text);

            message.SetText();
        }


        private static void HandleCardMessage()
        {
            var message = MessageFactory.GetMessage<CardMessage>(MessageType.Card);

            message.SetTitle();
        }
    }


    public class MessageFactory<TMessage>
        : IMessageFactory<Envelope<TMessage>>
    {
        public IEnvelope<TMessage> GetMessage(MessageType messageType)
            where TMessage : IMessage
        {
            switch (messageType)
            {
                case MessageType.Card:
                {
                    var msg = new Envelope<CardMessage>(new CardMessage());
                    return msg;
                }
                case MessageType.Text:
                {
                    return new TextMessage();
                }
                default:
                {
                    throw new Exception("");
                }
            }
        }
    }

    public interface IMessageFactory<TEnvelope>
    {
        IEnvelope<TMessage> GetMessage(MessageType messageType)
            where TMessage : IMessage;
    }

    public interface IEnvelope<out TMessage>
        where TMessage : IMessage
    {
        TMessage Message { get; }
    }

    public class Envelope<TMessage>
        : IEnvelope<TMessage>
            where TMessage : IMessage
    {
        public TMessage Message { get; }


        public Envelope(TMessage message)
        {
            Message = message;
        }
    }


    public class TextMessage : IMessage
    {
        public void SetText()
        {
            Console.WriteLine("Set text");
        }
    }

    public class CardMessage : IMessage
    {
        public void SetTitle()
        {
            Console.WriteLine("Set title");
        }
    }

    public interface IMessage
    {
    }


    public enum MessageType
    {
        Card,
        Text
    }
}
