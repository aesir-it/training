namespace IoCCHost
{
    public interface IExtraExtraDataService
    {
        string GetData();
    }

    public class ExtraExtraDataService : IExtraExtraDataService
    {
        public string GetData()
        {
            return "extrraaaa data";
        }
    }
}