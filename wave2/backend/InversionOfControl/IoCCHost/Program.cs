﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;


namespace IoCCHost
{
    public static class Program
    {
        private static Dictionary<Type, List<Type>> _registrationLookup = new Dictionary<Type, List<Type>>();


        public static void Main()
        {
            const string service01Name = "serviceVersion01";
            const string service02Name = "serviceVersion02";

            var containerBuilder = new ContainerBuilder();


            containerBuilder
                .RegisterType<ExtraDataService>()
                .As<IExtraDataService>()
                .SingleInstance();

            containerBuilder
                .RegisterType<ServiceVersion02>()
                .As<IService>()
                .Named<IService>(service02Name);

            containerBuilder
                .RegisterType<Form>()
                //.As<Form>()
                .AsSelf()
                .As<IForm>()
                .SingleInstance();

            //containerBuilder
            //    .RegisterType<Form>()
            //    //.As<Form>()
            //    //.AsSelf()
            //    .As<IForm>()
            //    .SingleInstance();

            containerBuilder
                .RegisterType<ServiceVersion01>()
                .As<IService>()
                .Named<IService>(service01Name)
                .PreserveExistingDefaults();

            containerBuilder
                .RegisterType<MultiForm>()
                .As<IForm>();

            containerBuilder
                .RegisterType<MultiSpecificForm>()
                .As<IForm>()
                .WithParameter(
                    (p, c) => p.Name == "service01",
                    (p, c) => c.ResolveNamed<IService>(service01Name))
                .WithParameter(
                    (p, c) => p.Name == "service02",
                    (p, c) => c.ResolveNamed<IService>(service02Name));

            containerBuilder
                .RegisterType<ExtraExtraDataService>()
                .As<IExtraExtraDataService>()
                .SingleInstance();

            containerBuilder.RegisterModule<ItemModule>();

            var inversionOfControlContainer = containerBuilder.Build();


            // Own IoC container.
            _registrationLookup[typeof(Form)] = new List<Type> { typeof(Form) };
            _registrationLookup[typeof(IService)] = new List<Type> { typeof(ServiceVersion01) };
            var f = Resolve<Form>();
            f.Show();


            //IService service = new ServiceVersion01();

            //var form = new Form(service);

            var form01 = inversionOfControlContainer.Resolve<IForm>();
            var form02 = inversionOfControlContainer.Resolve<Form>();



            form01.Show();
            form02.Show();

            form01.AddItem("Item01");
            form01.AddItem("Item02");
            form01.AddItem(ItemType.Item01);
            form01.AddItem(ItemType.Item02);
            form01.AddAllItems();
            //form01.AddItem("Item03");

            Console.ReadKey();


            inversionOfControlContainer.Dispose();
        }


        private static T Resolve<T>()
        {
            return (T) Resolve(typeof (T));
        }


        private static object Resolve(Type t)
        {
            var registeredType = GetRegistration(t);

            var paraInstances = new List<object>();

            var ctors = registeredType.GetConstructors();
            foreach (var ctor in ctors)
            {
                var paras = ctor.GetParameters();
                foreach (var para in paras)
                {
                    var paraInstance = Resolve(para.ParameterType);

                    paraInstances.Add(paraInstance);
                }

                var instance = ctor.Invoke(paraInstances.ToArray());

                return instance;
            }
       
            throw new KeyNotFoundException();
        }


        private static Type GetRegistration(Type type)
        {
            return _registrationLookup[type].Last();
        }
    }

    public class ItemModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<IoCCItemFactory>()
                .As<IItemFactory>();

            builder
                .RegisterType<Item01>()
                .Named<IItem>("Item01")
                .Keyed<IItem>(ItemType.Item01);

            builder
                .RegisterType<Item02>()
                .Named<IItem>("Item02")
                .Keyed<IItem>(ItemType.Item02);
        }
    }


    public class Form
        : IForm
    {
        private readonly IService _service;
        private Guid _uuid;



        public Form(IService service)
        {
            _service = service;

            _uuid = Guid.NewGuid();
        }


        public void Show()
        {
            var data = _service.GetData();

            Console.WriteLine($"Form {_uuid}: {data}");
        }


        public void AddItem(string typeName)
        {
        }


        public void AddItem(ItemType itemType)
        {
        }


        public void AddAllItems()
        {
            
        }
    }



    public class MultiForm
        : IForm
    {
        private readonly IEnumerable<IService> _services;



        public MultiForm(IEnumerable<IService> services)
        {
            _services = services;
        }


        public void Show()
        {
            var sb = new StringBuilder();
            _services
                .ToList()
                .ForEach(service => sb.Append($"{service.GetData()} -"));

            var data = sb.ToString();

            Console.WriteLine(data);
        }


        public void AddItem(string typeName)
        {
        }


        public void AddItem(ItemType itemType)
        {
        }


        public void AddAllItems()
        {

        }
    }



    public class MultiSpecificForm
        : IForm
    {
        private readonly IService _service01;
        private readonly IService _service02;
        private readonly IItemFactory _itemFactory;



        public MultiSpecificForm(IService service01, IService service02, IItemFactory itemFactory)
        {
            _service01 = service01;
            _service02 = service02;
            _itemFactory = itemFactory;
        }


        public void Show()
        {
            var sb = new StringBuilder();
            sb.Append(_service01.GetData());
            sb.Append(" - ");
            sb.Append(_service02.GetData());

            var data = sb.ToString();

            Console.WriteLine(data);
        }


        public void AddItem(string typeName)
        {
            var item = _itemFactory.Create(typeName);

            Console.WriteLine(item.TypeName);
        }


        public void AddItem(ItemType itemType)
        {
            var item = _itemFactory.Create(itemType);

            Console.WriteLine(item.TypeName);
        }


        public void AddAllItems()
        {
            var vals = Enum.GetValues(typeof(ItemType));
            foreach (ItemType val in vals)
            {
                AddItem(val);
            }
        }
    }


    public interface IItem
    {
        string TypeName { get; }
    }



    class Item01 : IItem
    {
        public string TypeName => "Item01";
    }



    class Item02 : IItem
    {
        private readonly IExtraDataService _extraDataService;
        public string TypeName => "Item02" + _extraDataService.GetExtraData();


        public Item02(IExtraDataService extraDataService)
        {
            _extraDataService = extraDataService;
        }
    }



    public interface IForm
    {
        void Show();


        void AddItem(string typeName);


        void AddItem(ItemType itemType);
        void AddAllItems();
    }



    public interface IService
    {
        string GetData();
    }



    public class ServiceVersion01
        : IService
    {
        public ServiceVersion01()
        {
        }


        public string GetData()
        {
            return "Version 01";
        }
    }



    public class ServiceVersion02
        : IService
    {
        private readonly IExtraDataService _extraDataService;



        public ServiceVersion02(IExtraDataService extraDataService)
        {
            _extraDataService = extraDataService;
        }


        public string GetData()
        {
            return "Version 02 -> " + _extraDataService.GetExtraData();
        }
    }



    public interface IExtraDataService
    {
        string GetExtraData();
    }



    public class ExtraDataService
        : IExtraDataService
    {
        private readonly IExtraExtraDataService _extraExtraDataService;



        public ExtraDataService(IExtraExtraDataService extraExtraDataService)
        {
            _extraExtraDataService = extraExtraDataService;
        }


        public string GetExtraData()
        {
            return "need input " + _extraExtraDataService.GetData();
        }
    }
}