﻿using Autofac;

namespace IoCCHost
{
    public interface IItemFactory
    {
        IItem Create(string typeName);


        IItem Create(ItemType itemType);
    }



    public class IoCCItemFactory
        : IItemFactory
    {
        private readonly IComponentContext _container;



        public IoCCItemFactory(IComponentContext container)
        {
            _container = container;
        }


        public IItem Create(string typeName)
        {
            return _container.ResolveNamed<IItem>(typeName);
        }


        public IItem Create(ItemType itemType)
        {
            return _container.ResolveKeyed<IItem>(itemType);
        }
    }



    public enum ItemType
    {
        Item01,
        Item02
    }
}