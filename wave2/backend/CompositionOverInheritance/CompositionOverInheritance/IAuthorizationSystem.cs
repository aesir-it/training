namespace CompositionOverInheritance
{
    public interface IAuthorizationSystem
    {
        bool IsAllowed(string userName);
    }

    class AuthorizationSystem : IAuthorizationSystem
    {
        public bool IsAllowed(string userName)
        {
            return userName != "Dwight";
        }
    }
}