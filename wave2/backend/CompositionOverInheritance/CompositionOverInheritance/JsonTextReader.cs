namespace CompositionOverInheritance
{
    public class JsonTextReader
        : ITextReader
    {
        public string Read()
        {
            return "{\"text\": \"read\"";
        }
    }
}