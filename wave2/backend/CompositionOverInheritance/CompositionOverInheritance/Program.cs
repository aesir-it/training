﻿using System;


namespace CompositionOverInheritance
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // BAD
            //ITextReader textReader = new TextFileReader();
            //ITextReader textReader = new SecureTextFileReader();
            //ITextReader textReader = new LoggingSecureTextFileReader();
            //ITextReader textReader = new XmlFileReader();
            //ITextReader textReader = new SecureXmlFileReader();


            ////ITextReader textFileReader = new TextFileReader();
            //ITextReader textXmlReader = new XmlFileReader();

            //ITextReader compositeSecureTextFileReader = new CompositeSecureFileReader(textXmlReader);
            //ITextReader loggingCompositeSecureTextFileReader = new CompositeLoggingTextReader(compositeSecureTextFileReader);
            //ITextReader authLoggingCompositeSecureTextFileReader = new CompositeAuthorizationTextReader(loggingCompositeSecureTextFileReader, new AuthorizationSystem(), "Dwight");


            //ITextReader textReader = authLoggingCompositeSecureTextFileReader;


            var builder = new TextReaderBuilder();

            // NON FLUENT
            //builder.UseText();
            //builder.UseLogging();
            ////builder.UseEncryption();
            ////builder.UseAuthorization(new AuthorizationSystem(), "Bjorn");
            //var textReader = builder.Build();


            //// FLUENT
            //var textReader =
            //    builder
            //        .UseText()
            //        .UseLogging()
            //        .UseEncryption()
            //        //.UseAuthorization(new AuthorizationSystem(), "Bjorn")
            //        .Build();


            // FLUENT BETTER
            var betterBuilder = new BetterTextReaderBuilder();

            var textReader =
                betterBuilder
                    .UseJson()
                    .UseLogging()
                    .UseEncryption()
                    //.UseAuthorization(new AuthorizationSystem(), "Bjorn")
                    .Build();

            var text = textReader.Read();

            Console.WriteLine($"Text: {text}");
            Console.ReadKey();
        }
    }
}