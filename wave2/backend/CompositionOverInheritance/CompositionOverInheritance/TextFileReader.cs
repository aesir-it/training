using System;
using System.Linq;
using System.Runtime.InteropServices;

namespace CompositionOverInheritance
{
    public class TextFileReader
        : ITextReader
    {
        public bool IsSecure { get; set; }


        public virtual string Read()
        {
            return "Text read";
        }
    }


    public class CompositeSecureFileReader
        : ITextReader
    {
        private readonly ITextReader _textReader;



        public CompositeSecureFileReader(ITextReader textReader)
        {
            _textReader = textReader;
        }


        public string Read()
        {
            var textRead = _textReader.Read();

            textRead = new string(textRead.Reverse().ToArray());

            return textRead;
        }
    }



    public class SecureTextFileReader
        : TextFileReader
    {
        public override string Read()
        {
            var textRead = base.Read();

            textRead = new string(textRead.Reverse().ToArray());

            return textRead;
        }
    }


    public class CompositeLoggingTextReader
        : ITextReader
    {
        private readonly ITextReader _textReader;



        public CompositeLoggingTextReader(ITextReader textReader)
        {
            _textReader = textReader;
        }


        public string Read()
        {
            Console.WriteLine("Before");

            var textRead = _textReader.Read();

            Console.WriteLine("after");

            return textRead;
        }
    }


    public class CompositeAuthorizationTextReader
        : ITextReader
    {
        private readonly ITextReader _textReader;
        private readonly IAuthorizationSystem _authorizationSystem;
        private readonly string _userName;



        public CompositeAuthorizationTextReader(ITextReader textReader, IAuthorizationSystem authorizationSystem, string userName)
        {
            _textReader = textReader;
            _authorizationSystem = authorizationSystem;
            _userName = userName;
        }


        public string Read()
        {
            if (_authorizationSystem.IsAllowed(_userName))
            {
                return "NOT ALLOWED";
            }


            var textRead = _textReader.Read();

            return textRead;
        }
    }


    public class LoggingSecureTextFileReader
        : SecureTextFileReader
    {
        public override string Read()
        {
            Console.WriteLine("Before");

            var textRead = base.Read();

            Console.WriteLine("after");

            return textRead;
        }
    }
}