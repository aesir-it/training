﻿namespace CompositionOverInheritance
{
    public class TextReaderBuilder
    {
        private ITextReader _textReader;



        public TextReaderBuilder UseText()
        {
            _textReader = new TextFileReader();

            return this;
        }


        public TextReaderBuilder UseXml()
        {
            _textReader = new XmlFileReader();

            return this;
        }


        public TextReaderBuilder UseLogging()
        {
            _textReader = new CompositeLoggingTextReader(_textReader);

            return this;
        }


        public TextReaderBuilder UseEncryption()
        {
            _textReader = new CompositeSecureFileReader(_textReader);

            return this;
        }


        public TextReaderBuilder UseAuthorization(IAuthorizationSystem authorizationSystem, string userName)
        {
            _textReader = new CompositeAuthorizationTextReader(_textReader, authorizationSystem, userName);

            return this;
        }


        public ITextReader Build()
        {
            return _textReader;
        }
    }
}