﻿namespace CompositionOverInheritance
{
    public class XmlFileReader
        : ITextReader
    {
        public virtual string Read()
        {
            return "<xml>read</xml>";
        }
    }
}