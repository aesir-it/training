namespace CompositionOverInheritance
{
    public interface ITextReader
    {
        string Read();
    }
}