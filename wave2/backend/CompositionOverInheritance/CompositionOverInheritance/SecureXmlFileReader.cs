using System.Linq;

namespace CompositionOverInheritance
{
    public class SecureXmlFileReader
        : XmlFileReader
    {
        public override string Read()
        {
            var textRead = base.Read();

            textRead = new string(textRead.Reverse().ToArray());

            return textRead;
        }
    }
}