﻿namespace CompositionOverInheritance
{
    public class BetterTextReaderBuilder
    {
        private ITextReader _textReader;



        public ExtraServicesTextReaderBuilder UseText()
        {
            _textReader = new TextFileReader();

            return new ExtraServicesTextReaderBuilder(_textReader);
        }


        public ExtraServicesTextReaderBuilder UseXml()
        {
            _textReader = new XmlFileReader();

            return new ExtraServicesTextReaderBuilder(_textReader);
        }


        public ExtraServicesTextReaderBuilder UseJson()
        {
            _textReader = new JsonTextReader();

            return new ExtraServicesTextReaderBuilder(_textReader);
        }
    }


    public class ExtraServicesTextReaderBuilder
    {
        private ITextReader _textReader;


        public ExtraServicesTextReaderBuilder(ITextReader textReader)
        {
            _textReader = textReader;
        }


        public ExtraServicesTextReaderBuilder UseLogging()
        {
            _textReader = new CompositeLoggingTextReader(_textReader);

            return this;
        }


        public ExtraServicesTextReaderBuilder UseEncryption()
        {
            _textReader = new CompositeSecureFileReader(_textReader);

            return this;
        }


        public ExtraServicesTextReaderBuilder UseAuthorization(IAuthorizationSystem authorizationSystem, string userName)
        {
            _textReader = new CompositeAuthorizationTextReader(_textReader, authorizationSystem, userName);

            return this;
        }


        public ITextReader Build()
        {
            return _textReader;
        }
    }
}