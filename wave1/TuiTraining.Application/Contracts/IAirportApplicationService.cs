﻿using TuiTraining.Dto;

namespace TuiTraining.Application.Contracts
{
    public interface IAirportApplicationService
    {
        AirportDto Get(int airportId);


        void Update(AirportDto airportDto);
    }
}