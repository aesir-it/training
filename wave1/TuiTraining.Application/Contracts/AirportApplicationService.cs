using TuiTraining.Contracts;
using TuiTraining.Domain.Model;
using TuiTraining.Domain.Repositories;
using TuiTraining.Domain.Repositories.Contracts;
using TuiTraining.Dto;

namespace TuiTraining.Application.Contracts
{
    public class AirportApplicationService
        : IAirportApplicationService
    {
        private readonly IAirportRepository _airportRepository;
        private readonly IMapper<Airport, AirportDto> _airportDtoMapper;
        private readonly IMapper<AirportDto, Airport> _airportMapper;



        public AirportApplicationService(IAirportRepository airportRepository, IMapper<Airport, AirportDto> airportDtoMapper, IMapper<AirportDto, Airport> airportMapper)
        {
            _airportRepository = airportRepository;
            _airportDtoMapper = airportDtoMapper;
            _airportMapper = airportMapper;
        }


        public AirportDto Get(int airportId)
        {
            var airport = _airportRepository.Get(airportId);

            var airportDto = _airportDtoMapper.Map(airport);

            return airportDto;
        }


        public void Update(AirportDto airportDto)
        {
            var airport = _airportMapper.Map(airportDto);

            _airportRepository.Update(airport);
        }
    }
}