﻿using System;
using System.Collections.Generic;
using System.Linq;
using TuiTraining.Domain.Model;
using TuiTraining.Domain.Procedures;
using TuiTraining.Domain.Services;


namespace TuiTraining.Cli.Host
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var brusselsAirport = new Airport(1, "Brussel");
            var charleroiAirport = new Airport(2, "Charleroi");
            var oostendeAirport = new Airport(3, "Oostende");
            var deurneAirport = new Airport(4, "Deurne");

            var airports =
                new List<Airport>
                {
                    brusselsAirport,
                    charleroiAirport,
                    oostendeAirport,
                    deurneAirport
                };

            Console.WriteLine("Found following airports:");
            airports.ForEach(x => Console.WriteLine($"\tAirport {x}"));
            Console.WriteLine();

            var randomizer = new Random((int) DateTime.Now.Ticks);

            var flightPlans =
                airports
                    .SelectMany(x => airports, (startAirport, stopAirport) => new { StartAirport = startAirport, StopAirport = stopAirport })
                    .Where(x => x.StartAirport != x.StopAirport)
                    .Select(x => new FlightPlan(x.StartAirport, x.StopAirport, randomizer.Next(50, 251)))
                    .ToList();

            Console.WriteLine("Calculated flightplans:");
            flightPlans.ForEach(x => Console.WriteLine($"\tFrom airport {x.StartAirport.Name} to airport {x.StopAirport.Name} spanning {x.Distance} kms."));
            Console.WriteLine();

            var ulm001 = new Ulm("The Spear", "ULM001", deurneAirport);
            var ulm002 = new Ulm("Nimbus", "ULM002", oostendeAirport);
            var helicopter = new Helicopter("Airwolf", "AIR", oostendeAirport);
            var hotAirBalloon = new HotAirBalloon("Hotty", "HOT", charleroiAirport);
            var airForceOne = new Airplane("Air Force One", "AF1", brusselsAirport);
            var hitanic = new Airplane("Hitanic", "HI", brusselsAirport);

            var flyingObjects =
                new List<FlyingObject>
                {
                    ulm001,
                    ulm002,
                    helicopter,
                    hotAirBalloon,
                    airForceOne,
                    hitanic
                };

            Console.WriteLine("Found flying objects:");
            flyingObjects.ForEach(x => Console.WriteLine($"\t{x.Name} of type {x.GetType().Name} with current airport {x.CurrentAirport}"));

            var flightEngineService = new FlightEngineService(flyingObjects, null, new RandomizerProcedureFactory(), null);
            flightEngineService.Run();

            Console.WriteLine("Press any key to quit...");
            Console.ReadKey();
        }
    }
}