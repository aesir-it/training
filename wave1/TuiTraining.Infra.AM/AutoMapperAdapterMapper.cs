﻿using TuiTraining.Contracts;

namespace TuiTraining.Infra.AM
{
    public class AutoMapperAdapterMapper<TIn, TOut>
        : IMapper<TIn, TOut>
    {
        public TOut Map(TIn source)
        {
            return AutoMapper.Mapper.Map<TIn, TOut>(source);
        }
    }


    public class MapConfiguration<TIn, TOut>
    {
        public static void Configure()
        {
            AutoMapper.Mapper.CreateMap<TIn, TOut>();
            AutoMapper.Mapper.CreateMap<TOut, TIn>();
        }
    }
}