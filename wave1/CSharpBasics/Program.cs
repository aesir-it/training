﻿using System;
using System.Collections;


namespace CSharpBasics
{
    struct MyStruct
    {
        public int a;
        public short b;
        public char c;
        public string s;
        public float f;
        public double d;
        public decimal e;


        public void Test()
        {
            Console.WriteLine(a);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            int a = 666;
            short b = 16;
            char c = 'c';
            string s = "dwight";
            float f = 6.66f;
            double d = 6.66;
            decimal e = 78m;

            Console.WriteLine(a);
            ChangeIt(ref a);
            Console.WriteLine(a);

            double doubled;
            var abc = DoubleIt(a, out doubled);
            Console.WriteLine(abc);

            DoMultiple(a, b, c, s, f);

            var objarr = new object[] { a, b, c, s, f };
            DoMultiple(objarr);

            Console.ReadKey();

            MyStruct m = new MyStruct();
            m.a = 123;

            DoStruct(ref m);
        }

        private static void DoStruct(ref MyStruct myStruct)
        {
            myStruct.a = 456;
        }


        private static void DoMultiple(params object[] parameters)
        {
            for (int i = 0; i < parameters.Length; i++)
            {
                var parameter = parameters[i];

                Console.WriteLine(String.Format("Value is {0}", parameter));
                Console.WriteLine($"Value {nameof(parameters)} is {parameter}");
            }

            foreach (var parameter in parameters)
            {
                
            }

            var enumerator = parameters.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current);
            }

            if (parameters.Length != 0)
            {
                Console.WriteLine("It's not empty");
            }

            var s = "aa" + "bb";
            var s2 = String.Concat("aa", "bb");

            IEnumerable a;
        }


        static void ChangeIt(ref int val)
        {
            val = 133;
        }


        static int DoubleIt(int val, out double doubled)
        {
            var doubleIt = val * 2;

            doubled = doubleIt;

            return doubleIt;
        }
    }
}