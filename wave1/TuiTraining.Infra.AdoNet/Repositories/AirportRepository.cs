﻿using System;
using System.Data.SqlClient;
using TuiTraining.Domain.Model;
using TuiTraining.Domain.Repositories;
using TuiTraining.Domain.Repositories.Contracts;
using TuiTraining.Infra.AdoNet.Datasets;

namespace TuiTraining.Infra.AdoNet.Repositories
{
    public class AirportRepository
        : IAirportRepository
    {
        public Airport Get(int airportId)
        {
            var tds = new AirportTds();

            DoUnderConnection(ta => ta.FillBy(tds.Airport, airportId));

            var row = tds.Airport.FindByAirportId(airportId);

            return new Airport(row.AirportId, row.Name);
        }


        public void Update(Airport airport)
        {
            //var tds = new AirportTds();

            //DoUnderConnection(
            //    ta =>
            //    {
            //        ta.FillBy(tds.Airport, airport.AirportId);
            //        var row = tds.Airport.FindByAirportId(airport.AirportId);

            //        row.Name = airport.Name;

            //        ta.Update(tds.Airport);
            //    });

            UpdateUsingTableAdapterQuery(airport);
        }


        public void UpdateUsingTableAdapterQuery(Airport airport)
        {
            DoUnderConnection(ta => ta.UpdateQuery(airport.Name, airport.AirportId));
        }


        private void DoUnderConnection(Action<Datasets.AirportTdsTableAdapters.AirportTableAdapter> action)
        {
            const string connectionString = "Server=(local);Database=TuiTraining;Trusted_Connection=True;";
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var ta =
                    new Datasets.AirportTdsTableAdapters.AirportTableAdapter
                    {
                        Connection = connection
                    };

                action(ta);

                connection.Close();
            }
        }
    }
}