﻿using TuiTraining.Domain.Model;

namespace TuiTraining.Domain.Services.Contracts
{
    public interface IFlightPlanCalculatorService
    {
        FlightPlan CalculateFlightPlan(FlyingObject flyingObject);
    }
}