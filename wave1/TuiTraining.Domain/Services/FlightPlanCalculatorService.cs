﻿using System.Linq;
using TuiTraining.Domain.Model;
using TuiTraining.Domain.Repositories.Contracts;
using TuiTraining.Domain.Services.Contracts;

namespace TuiTraining.Domain.Services
{
    public class FlightPlanCalculatorService
        : IFlightPlanCalculatorService
    {
        private readonly IFlightPlanRepository _flightPlanRepository;



        public FlightPlanCalculatorService(IFlightPlanRepository flightPlanRepository)
        {
            _flightPlanRepository = flightPlanRepository;
        }


        public FlightPlan CalculateFlightPlan(FlyingObject flyingObject)
        {
            var suitableFlightPlan =
                _flightPlanRepository
                    .All()
                    .FirstOrDefault(x => x.IsFlightPlanSuitable(flyingObject));

            return suitableFlightPlan ?? FlightPlan.Null;
        }
    }
}