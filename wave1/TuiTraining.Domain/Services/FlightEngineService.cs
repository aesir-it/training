﻿using System;
using System.Collections.Generic;
using System.Linq;
using TuiTraining.Domain.Iterators.Contracts;
using TuiTraining.Domain.Model;
using TuiTraining.Domain.Procedures.Contracts;
using TuiTraining.Domain.Services.Contracts;


namespace TuiTraining.Domain.Services
{
    public class FlightEngineService
    {
        private readonly List<FlyingObject> _flyingObjects;
        private readonly IFlightPlanCalculatorService _flightPlanCalculatorService;
        private readonly IProcedureFactory _procedureFactory;
        private readonly IFlyingObjectPredicateIterator _flyingObjectPredicateIterator;



        public FlightEngineService(List<FlyingObject> flyingObjects, IFlightPlanCalculatorService flightPlanCalculatorService, IProcedureFactory procedureFactory, IFlyingObjectPredicateIterator flyingObjectPredicateIterator)
        {
            _flyingObjects = flyingObjects;
            _flightPlanCalculatorService = flightPlanCalculatorService;
            _procedureFactory = procedureFactory;
            _flyingObjectPredicateIterator = flyingObjectPredicateIterator;
        }


        public void Run()
        {
            while (_flyingObjectPredicateIterator.PredicateIsValid(_flyingObjects))
            {
                foreach (var flyingObject in _flyingObjects.Where(x => x.HasNotCrashed))
                {
                    var startProcedure = _procedureFactory.CreateStartProcedure();
                    var stopProcedure = _procedureFactory.CreateStopProcedure();

                    var suitableFlightPlan = _flightPlanCalculatorService.CalculateFlightPlan(flyingObject);

                    try
                    {
                        flyingObject.Fly(suitableFlightPlan, startProcedure, stopProcedure);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }
    }
}