﻿using System.Collections.Generic;
using TuiTraining.Domain.Model;

namespace TuiTraining.Domain.Repositories.Contracts
{
    public interface IFlightPlanRepository
    {
        IEnumerable<FlightPlan> All();
    }
}