﻿using TuiTraining.Domain.Model;

namespace TuiTraining.Domain.Repositories.Contracts
{
    public interface IAirportRepository
    {
        Airport Get(int airportId);


        void Update(Airport airport);
    }
}