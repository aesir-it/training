﻿using System.Collections.Generic;
using System.Linq;
using TuiTraining.Domain.Iterators.Contracts;
using TuiTraining.Domain.Model;

namespace TuiTraining.Domain.Iterators
{
    public class UntillAllFlyingObjectsCrashedIterator
        : IFlyingObjectPredicateIterator
    {
        public bool PredicateIsValid(IEnumerable<FlyingObject> flyingObjects)
        {
            return flyingObjects.Any(x => x.HasNotCrashed);
        }
    }
}