﻿using System.Collections.Generic;
using TuiTraining.Domain.Model;

namespace TuiTraining.Domain.Iterators.Contracts
{
    public interface IFlyingObjectPredicateIterator
    {
        bool PredicateIsValid(IEnumerable<FlyingObject> flyingObjects);
    }
}