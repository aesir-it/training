using System;
using TuiTraining.Domain.Procedures.Contracts;


namespace TuiTraining.Domain.Procedures
{
    public class RandomizerProcedureFactory
        : IProcedureFactory
    {
        private readonly Random _randomizer;



        public RandomizerProcedureFactory()
        {
            _randomizer = new Random((int) DateTime.Now.Ticks);
        }


        public IFlyingObjectVisitor CreateStartProcedure()
        {
            return CreateProcedure<StartProcedure>();
        }


        public IFlyingObjectVisitor CreateStopProcedure()
        {
            return CreateProcedure<StopProcedure>();
        }


        private IFlyingObjectVisitor CreateProcedure<T>()
            where T : IFlyingObjectVisitor, new()
        {
            var type = _randomizer.Next(0, 2);

            if (type == 0)
            {
                return new T();
            }

            return new CrashingProcedure();
        }
    }
}