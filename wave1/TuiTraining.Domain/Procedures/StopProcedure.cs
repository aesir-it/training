﻿using TuiTraining.Domain.Model;
using TuiTraining.Domain.Procedures.Contracts;

namespace TuiTraining.Domain.Procedures
{
    public class StopProcedure
        : IFlyingObjectVisitor
    {
        public void Visit(Airplane airplane)
        {
            airplane.InformPassengers();
            airplane.OpenLandingGear();
            airplane.Land();
        }


        public void Visit(HotAirBalloon hotAirBalloon)
        {
            hotAirBalloon.CloseGas(100);
            hotAirBalloon.CloseGas(75);
            hotAirBalloon.CloseGas(50);
            hotAirBalloon.CloseGas(25);
            hotAirBalloon.CloseGas(0);
        }


        public void Visit(Helicopter helicopter)
        {
            helicopter.LowerWheels();
            helicopter.RevertPropeller();
            helicopter.Land();
        }


        public void Visit(Ulm ulm)
        {
            ulm.LowerFlaps();
            ulm.LowerNose();
        }
    }
}