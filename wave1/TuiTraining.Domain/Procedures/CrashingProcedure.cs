using TuiTraining.Domain.Model;
using TuiTraining.Domain.Procedures.Contracts;


namespace TuiTraining.Domain.Procedures
{
    public class CrashingProcedure
        : IFlyingObjectVisitor
    {
        public void Visit(Airplane airplane)
        {
            airplane.Crash();
        }


        public void Visit(HotAirBalloon hotAirBalloon)
        {
            hotAirBalloon.Crash();
        }


        public void Visit(Helicopter helicopter)
        {
            helicopter.Crash();
        }


        public void Visit(Ulm ulm)
        {
            ulm.Crash();
        }
    }
}