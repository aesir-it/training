﻿namespace TuiTraining.Domain.Procedures.Contracts
{
    public interface IProcedureFactory
    {
        IFlyingObjectVisitor CreateStartProcedure();


        IFlyingObjectVisitor CreateStopProcedure();
    }
}