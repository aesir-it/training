﻿using TuiTraining.Domain.Model;

namespace TuiTraining.Domain.Procedures.Contracts
{
    public interface IFlyingObjectVisitor
    {
        void Visit(Airplane airplane);


        void Visit(HotAirBalloon hotAirBalloon);


        void Visit(Helicopter helicopter);


        void Visit(Ulm ulm);
    }
}