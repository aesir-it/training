﻿using TuiTraining.Domain.Model;
using TuiTraining.Domain.Procedures.Contracts;

namespace TuiTraining.Domain.Procedures
{
    public class StartProcedure
        : IFlyingObjectVisitor
    {
        public void Visit(Airplane airplane)
        {
            airplane.Refuel();
            airplane.LoadLuggage();
            airplane.BoardPassengers();
            airplane.DoSafetyChecks();
            airplane.TakeOff();
        }


        public void Visit(HotAirBalloon hotAirBalloon)
        {
            hotAirBalloon.ConnectGas();
            hotAirBalloon.LoadBallast();
            hotAirBalloon.BoardPassengers();
            hotAirBalloon.Rise();
        }


        public void Visit(Helicopter helicopter)
        {
            helicopter.Refuel();
            helicopter.LoadCrew();
            helicopter.DoSafetyChecks();
            helicopter.LiftOff();
        }


        public void Visit(Ulm ulm)
        {
            ulm.ConnectWithCarrier();
            ulm.BoardPilots();
            ulm.TakeOff();
            ulm.Disconnect(500);
        }
    }
}