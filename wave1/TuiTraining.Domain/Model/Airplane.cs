﻿using System;
using TuiTraining.Domain.Procedures.Contracts;


namespace TuiTraining.Domain.Model
{
    public class Airplane
        : FlyingObject
    {
        public Airplane(string name, string code, Airport currentAirport)
            : base(name, code, currentAirport)
        {
        }


        public void Refuel()
        {
            Console.WriteLine($"Airplane {Name} is refuelling");
        }


        public void LoadLuggage()
        {
            Console.WriteLine($"Airplane {Name} is loading luggage");
        }


        public void BoardPassengers()
        {
            Console.WriteLine($"Airplane {Name} is boarding passengers");
        }


        public void DoSafetyChecks()
        {
            Console.WriteLine($"Airplane {Name} is doing safety checks");
        }


        public void TakeOff()
        {
            Console.WriteLine($"Airplane {Name} is taking off");
        }


        public void InformPassengers()
        {
            Console.WriteLine($"Airplane {Name} is informing passengers");
        }


        public void OpenLandingGear()
        {
            Console.WriteLine($"Airplane {Name} is opening landing gear");
        }


        public void Land()
        {
            Console.WriteLine($"Airplane {Name} is landing");
        }


        public override void Fly(FlightPlan suitableFlightPlan, IFlyingObjectVisitor startProcedure, IFlyingObjectVisitor stopProcedure)
        {
            startProcedure.Visit(this);

            Console.WriteLine($"Airplane {Name} is flying from {suitableFlightPlan.StartAirport.Name} to {suitableFlightPlan.StopAirport.Name}");

            stopProcedure.Visit(this);
        }
    }
}