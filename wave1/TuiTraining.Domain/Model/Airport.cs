namespace TuiTraining.Domain.Model
{
    public class Airport
    {
        public int AirportId { get; private set; }


        public string Name { get; private set; }



        public Airport(int airportId, string name)
        {
            AirportId = airportId;
            Name = name;
        }


        public override string ToString()
        {
            return $"{Name} Airport";
        }


        protected bool Equals(Airport other)
        {
            return AirportId == other.AirportId && string.Equals(Name, other.Name);
        }


        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Airport)obj);
        }


        public override int GetHashCode()
        {
            unchecked
            {
                return (AirportId * 397) ^ (Name != null ? Name.GetHashCode() : 0);
            }
        }
    }
}