﻿using System;
using TuiTraining.Domain.Procedures.Contracts;


namespace TuiTraining.Domain.Model
{
    public class HotAirBalloon
        : FlyingObject
    {
        public HotAirBalloon(string name, string code, Airport currentAirport)
            : base(name, code, currentAirport)
        {
        }


        public void ConnectGas()
        {
            Console.WriteLine($"Hot airballoon {Name} is connecting gas");
        }


        public void LoadBallast()
        {
            Console.WriteLine($"Hot airballoon {Name} is loading ballast");
        }


        public void BoardPassengers()
        {
            Console.WriteLine($"Hot airballoon {Name} is boarding passengers");
        }


        public void Rise()
        {
            Console.WriteLine($"Hot airballoon {Name} is rising");
        }


        public void CloseGas(int gasLevel)
        {
            Console.WriteLine($"Hot airballoon {Name} is closing gas to {gasLevel}");
        }


        public override void Fly(FlightPlan suitableFlightPlan, IFlyingObjectVisitor startProcedure, IFlyingObjectVisitor stopProcedure)
        {
            startProcedure.Visit(this);

            Console.WriteLine($"Hot airballoon {Name} is floating from {suitableFlightPlan.StartAirport.Name} to {suitableFlightPlan.StopAirport.Name}");

            stopProcedure.Visit(this);
        }
    }
}