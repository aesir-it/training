using System;
using TuiTraining.Domain.Procedures.Contracts;


namespace TuiTraining.Domain.Model
{
    public abstract class FlyingObject
    {
        private bool _isCrashed;


        public string Name { get; private set; }


        public string Code { get; private set; }


        public Airport CurrentAirport { get; private set; }


        public bool HasNotCrashed => !_isCrashed;



        protected FlyingObject(string name, string code, Airport currentAirport)
        {
            Name = name;
            Code = code;
            CurrentAirport = currentAirport;
        }


        public abstract void Fly(FlightPlan suitableFlightPlan, IFlyingObjectVisitor startProcedure, IFlyingObjectVisitor stopProcedure);


        public void Crash()
        {
            _isCrashed = true;

            throw new Exception($"{Name} is crashing...");
        }
    }
}