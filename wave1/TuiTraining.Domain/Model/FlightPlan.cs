using System;

namespace TuiTraining.Domain.Model
{
    public class FlightPlan
    {
        public static FlightPlan Null => new FlightPlan(new NullAirport(), new NullAirport(), 0);



        public Airport StartAirport { get; private set; }


        public Airport StopAirport { get; private set; }


        public int Distance { get; private set; }



        public FlightPlan(Airport startAirport, Airport stopAirport, int distance)
        {
            StartAirport = startAirport;
            StopAirport = stopAirport;
            Distance = distance;
        }


        public bool IsFlightPlanSuitable(FlyingObject flyingObject)
        {
            return StartAirport.Equals(flyingObject.CurrentAirport);
        }


        public override string ToString()
        {
            return $"{StartAirport.Name} <-> {StopAirport.Name} ({Distance}km) (Hash: {GetHashCode()})";
        }


        protected bool Equals(FlightPlan other)
        {
            return Equals(StartAirport, other.StartAirport) && Equals(StopAirport, other.StopAirport) && Distance == other.Distance;
        }


        public override bool Equals(object obj)
        {
            base.Equals(obj);
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;

            return Equals((FlightPlan)obj);
        }


        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (StartAirport != null ? StartAirport.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (StopAirport != null ? StopAirport.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ Distance;
                return hashCode;
            }
        }
    }
}