namespace TuiTraining.Domain.Model
{
    public class NullAirport
        : Airport
    {
        public NullAirport()
            : base(0, "Null airport")
        {
        }
    }
}