﻿using System;
using TuiTraining.Domain.Procedures.Contracts;


namespace TuiTraining.Domain.Model
{
    public class Ulm
        : FlyingObject
    {
        public Ulm(string name, string code, Airport currentAirport)
            : base(name, code, currentAirport)
        {
        }


        public void ConnectWithCarrier()
        {
            Console.WriteLine($"Ulm {Name} is connecting with carrier");
        }


        public void BoardPilots()
        {
            Console.WriteLine($"Ulm {Name} is boarding pilots");
        }


        public void TakeOff()
        {
            Console.WriteLine($"Ulm {Name} is taking off");
        }


        public void LowerFlaps()
        {
            Console.WriteLine($"Ulm {Name} is lowering flaps");
        }


        public void LowerNose()
        {
            Console.WriteLine($"Ulm {Name} is lowering nose");
        }


        public void Disconnect(int altitude)
        {
            Console.WriteLine($"Ulm {Name} is disconnecting at altitude {altitude}");
        }


        public override void Fly(FlightPlan suitableFlightPlan, IFlyingObjectVisitor startProcedure, IFlyingObjectVisitor stopProcedure)
        {
            startProcedure.Visit(this);

            Console.WriteLine($"Ulm {Name} is floating from {suitableFlightPlan.StartAirport.Name} to {suitableFlightPlan.StopAirport.Name}");

            stopProcedure.Visit(this);
        }
    }
}