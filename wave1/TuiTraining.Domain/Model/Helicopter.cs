﻿using System;
using TuiTraining.Domain.Procedures.Contracts;


namespace TuiTraining.Domain.Model
{
    public class Helicopter
        : FlyingObject
    {
        public Helicopter(string name, string code, Airport currentAirport)
            : base(name, code, currentAirport)
        {
        }


        public void Refuel()
        {
            Console.WriteLine($"Helicopter {Name} is refuelling");
        }


        public void LoadCrew()
        {
            Console.WriteLine($"Helicopter {Name} is loading crew");
        }


        public void DoSafetyChecks()
        {
            Console.WriteLine($"Helicopter {Name} doing safety checks");
        }


        public void LiftOff()
        {
            Console.WriteLine($"Helicopter {Name} is lifting off");
        }


        public void LowerWheels()
        {
            Console.WriteLine($"Helicopter {Name} is lowering wheels");
        }


        public void RevertPropeller()
        {
            Console.WriteLine($"Helicopter {Name} is reverting propeller");
        }


        public void Land()
        {
            Console.WriteLine($"Helicopter {Name} is landing");
        }


        public override void Fly(FlightPlan suitableFlightPlan, IFlyingObjectVisitor startProcedure, IFlyingObjectVisitor stopProcedure)
        {
            startProcedure.Visit(this);

            Console.WriteLine($"Helicopter {Name} is hovering from {suitableFlightPlan.StartAirport.Name} to {suitableFlightPlan.StopAirport.Name}");

            stopProcedure.Visit(this);
        }
    }
}