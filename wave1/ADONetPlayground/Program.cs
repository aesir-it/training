﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ADONetPlayground
{
    class Program
    {
        static void Main(string[] args)
        {
            const string connectionString = "Server=(local);Database=TuiTraining;Trusted_Connection=True;";
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                //var selectCommand = connection.CreateCommand();
                //selectCommand.CommandType = CommandType.Text;
                //selectCommand.CommandText = "select AirportId, Name from Airport;";
                //selectCommand.CommandText = "select * from Airport where Name like @name;";
                //var parameter =
                //    new SqlParameter
                //    {
                //        ParameterName = "name",
                //        Value = "B%",
                //        SqlDbType = SqlDbType.NVarChar
                //    };
                //selectCommand.Parameters.Add(parameter);

                //var dataReader = command.ExecuteReader();

                //dataReader.Read();
                //while (dataReader.HasRows)
                //{
                //    var id = dataReader["AirportId"];
                //    var name = dataReader["Name"];

                //    dataReader.Read();
                //}

                //foreach (var row in dataReader)
                //{
                //    var idx = dataReader.GetOrdinal("AirportId");
                //    var id = dataReader.GetFieldValue<int>(idx);

                //    Console.WriteLine(id);
                //}

                //var insertCommand = connection.CreateCommand();
                //insertCommand.CommandType = CommandType.Text;
                //insertCommand.CommandText = "insert into Airport(Name) values (@name);";
                //var sqlParameter = new SqlParameter("name", SqlDbType.NVarChar, 250, "Name");
                //insertCommand.Parameters.Add(sqlParameter);


                //var dataset = new DataSet();

                //var dataAdapter =
                //    new SqlDataAdapter
                //    {
                //        SelectCommand = selectCommand,
                //        InsertCommand = insertCommand
                //    };
                //dataAdapter.Fill(dataset);

                //dataAdapter.TableMappings.Add("Airport", dataset.Tables[0].TableName);

                //foreach (DataTable table in dataset.Tables)
                //{
                //    foreach (DataRow row in table.Rows)
                //    {
                //        var id = row.Field<int>("AirportId");
                //        var name = row.Field<string>("Name");

                //        Console.WriteLine($"id is {id} with name {name}");
                //    }
                //}

                //var datatable = dataset.Tables[0];

                //var newRow = datatable.NewRow();
                ////newRow.SetField("AirportId", -1);
                //newRow.SetField("Name", "Honkong");

                //datatable.Rows.Add(newRow);

                //var changes = dataAdapter.Update(dataset);
                //Console.WriteLine($"Changed {changes} records!");

                //var dv = new DataView(dataset.Tables[0]);
                // dv.RowFilter = "AirportId > 1";

                // foreach (DataRowView row in dv)
                // {
                //     Console.WriteLine(row[0]);
                // }

                //var builder = new SqlCommandBuilder(dataAdapter);
                //var c1 = builder.DataAdapter.InsertCommand;
                //var c2 = builder.DataAdapter.UpdateCommand;

                var tds = new AirportTds();
                tds.Airport.AddAirportRow("Malaga");

                var ta = new AirportTdsTableAdapters.AirportTableAdapter();
                ta.Connection = connection;
                ta.Adapter.Update(tds);
                //ta.Fill(tds.Airport);


                connection.Close();
            }

            Console.WriteLine("press");
            Console.ReadKey();
        }
    }
}
