﻿namespace CSharpAdvanced.Core
{
    internal class InternalDog
        : PublicDog
    {
        private readonly int _age;



        private InternalDog()
        {
            
        }


        public InternalDog(int age)
        {
            _age = age;
        }
    }
}