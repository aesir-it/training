﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CSharpAdvanced.Core
{
    public class Dog
        : Animal
    {
    }


    public class Cat
        : Animal
    {
        
    }


    public abstract class Animal
    {
    }



    public interface IReader
    {
        string Read();
    }


    public abstract class ReaderBase
        : IReader
    {
        public abstract string Read();
    }
 

    public class FileReader
        : IReader
    {
        public string Read()
        {
            return "FileReader";
        }
    }


    public class SecureReader
        : IReader
    {
        private readonly IReader _reader;

        public SecureReader(IReader reader)
        {
            _reader = reader;
        }

        public string Read()
        {
            if (DateTime.Now.Minute % 2 == 0)
            {
                return "no rights";
            }

            return _reader.Read();
        }
    }


    public class XmlReader
        : IReader
    {
        public string Read()
        {
            return "XML FileReader";
        }
    }


    public class LoggingReader
        : IReader
    {
        private readonly IReader _reader;

        public LoggingReader(IReader reader)
        {
            _reader = reader;
        }

        public string Read()
        {
            Console.WriteLine("Log");


            return _reader.Read();
        }
    }



    public class IndexerUsage
    {
        private Dictionary<string, int> _lookup;


        public int this[string key]
        {
            get { return _lookup[key]; }
        }


        public int this[int idx]
        {
            get { return _lookup.ElementAt(idx).Value; }
        }

        public IndexerUsage()
        {
            _lookup = new Dictionary<string, int>();
            _lookup.Add("Dwight", 35);
            _lookup.Add("Dwight1", 35);
            _lookup.Add("Dwight2", 35);
            _lookup.Add("Dwight3", 35);
        }
    }
}