﻿namespace CSharpAdvanced.Core.Extensions
{
    public static class DogCloner
    {
        public static PublicDog Clone(this PublicDog dog)
        {
            return new PublicDog(dog.Age);
        }
    }
}