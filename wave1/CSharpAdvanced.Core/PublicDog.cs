﻿using System;

namespace CSharpAdvanced.Core
{
    public class PublicDog
    {
        private readonly int _age;
        private static int _height = 123;
        public int Age { get; } = 10;
        protected string _name;
        private DateTime _birthDateTime;

        public DateTime BirthDateTime => _birthDateTime;


        public PublicDog(int age)
        {
            _age = age;
        }

        public PublicDog()
        {

            _birthDateTime = new DateTime();

            var a = new NestedPublicDog();
        }


        static PublicDog()
        {
        }


        public static PublicDog Create(int age)
        {
            return new PublicDog(age);
        }


        public void Bark()
        {
            Console.WriteLine("Bark bark bark");
        }


        public PublicDog Clone(PublicDog pd)
        {
            return new PublicDog(pd.Age);
        }


        public int GetRandomAge()
        {
            return 123;
        }



        private class NestedPrivateDog
        {
        }


        protected internal class NestedProtectedInternalDog
        {

        }

        protected class NestedProtectedDog
        {
        }


        public class NestedPublicDog
        {
            public NestedPublicDog()
            {
                PublicDog pd = new PublicDog(666);
            }
        }



        public static class PublicDogFactory
        {
            public static PublicDog Create(int age)
            {
                return new PublicDog(age);
            }
        }


    }
}