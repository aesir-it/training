﻿using System.Web.Mvc;
using TuiTraining.Application.Contracts;
using TuiTraining.Contracts;
using TuiTraining.Domain.Model;
using TuiTraining.Domain.Repositories;
using TuiTraining.Dto;
using TuiTraining.Infra.AdoNet.Repositories;
using TuiTraining.Infra.AM;
using TuiTraining.Mvc.Host.Models;


namespace TuiTraining.Mvc.Host.Controllers
{
    public class AirportController : Controller
    {
        private readonly IAirportApplicationService _airportApplicationService;
        private readonly IMapper<AirportDto, AirportViewModel> _dtoToViewMapper;
        private readonly IMapper<AirportViewModel, AirportDto> _viewToDtoMapper;


        public AirportController(IAirportApplicationService airportApplicationService, IMapper<AirportDto, AirportViewModel> mapper, IMapper<AirportViewModel, AirportDto> mapper2)
        {
            _airportApplicationService = airportApplicationService;
            _dtoToViewMapper = mapper;
            _viewToDtoMapper = mapper2;
        }


        //public AirportController()
        //{
        //    IAirportRepository airportRepository = new AirportRepository();
        //    var airportDtoMapper = new AutoMapperAdapterMapper<Airport,AirportDto>();
        //    var airportMapper = new AutoMapperAdapterMapper<AirportDto, Airport>();

        //    _airportApplicationService = new AirportApplicationService(airportRepository, airportDtoMapper, airportMapper);
        //    _mapper = new AutoMapperAdapterMapper<AirportDto, AirportViewModel>();
        //    _mapper2 = new AutoMapperAdapterMapper<AirportViewModel, AirportDto>();
        //}


        // GET: Airport
        public ActionResult Index(int id)
        {
            var airportDto = _airportApplicationService.Get(id);

            var airportViewModel = _dtoToViewMapper.Map(airportDto);

            return View(airportViewModel);
        }


        [HttpPost]
        public ActionResult Index(AirportViewModel viewModel)
        {
            var airportDto = _viewToDtoMapper.Map(viewModel);

            _airportApplicationService.Update(airportDto);

            return Content("Updated!");
        }
    }
}