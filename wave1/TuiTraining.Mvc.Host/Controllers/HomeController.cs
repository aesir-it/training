﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace TuiTraining.Mvc.Host.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [ActionName("contact-us")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View("Contact");
        }


        public ActionResult Foo()
        {
            const string helloWorld = "hello world";

            return Content(helloWorld);
        }


        [HttpPost]
        public ActionResult Foo(string letterCase)
        {
            const string helloWorld = "hello world";
            var result = letterCase == "upper" ? helloWorld.ToUpper() : helloWorld.ToLower();

            return Content(result);
        }
    }
}