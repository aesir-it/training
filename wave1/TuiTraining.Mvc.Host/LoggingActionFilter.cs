﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TuiTraining.Mvc.Host
{
    public class LoggingActionFilter
        : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            System.Diagnostics.Debug.WriteLine($"executing {filterContext.RouteData.Route}");
        }
    }
}