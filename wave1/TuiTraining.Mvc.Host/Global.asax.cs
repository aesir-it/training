﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using TuiTraining.Application.Contracts;
using TuiTraining.Contracts;
using TuiTraining.Domain.Model;
using TuiTraining.Domain.Repositories;
using TuiTraining.Domain.Repositories.Contracts;
using TuiTraining.Dto;
using TuiTraining.Infra.AdoNet.Repositories;
using TuiTraining.Infra.AM;
using TuiTraining.Mvc.Host.Controllers;
using TuiTraining.Mvc.Host.Models;

namespace TuiTraining.Mvc.Host
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            MapConfiguration<AirportViewModel, AirportDto>.Configure();
            MapConfiguration<Airport, AirportDto>.Configure();

            var builder = new ContainerBuilder();

            builder
                .RegisterType<AirportRepository>()
                .As<IAirportRepository>()
                .SingleInstance();

            builder
                .RegisterType<AirportApplicationService>()
                .As<IAirportApplicationService>()
                .SingleInstance();

            builder
                .RegisterType<AirportController>()
                .AsSelf()
                .InstancePerRequest();

            builder
                .RegisterGeneric(typeof (AutoMapperAdapterMapper<,>))
                .As(typeof (IMapper<,>))
                .SingleInstance();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
