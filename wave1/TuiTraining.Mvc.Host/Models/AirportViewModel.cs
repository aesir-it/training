﻿using System.ComponentModel.DataAnnotations;

namespace TuiTraining.Mvc.Host.Models
{
    public class AirportViewModel
    {
        [Key]
        public int AirportId { get; set; }


        [Required]
        //[RegularExpression(@"\d{6,50}", ErrorMessage = "You stupid or what?")]
        public string Name { get; set; } 
    }
}