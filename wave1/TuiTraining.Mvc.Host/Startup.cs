﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TuiTraining.Mvc.Host.Startup))]
namespace TuiTraining.Mvc.Host
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
