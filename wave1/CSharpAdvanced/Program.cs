﻿using System;
using System.Collections;
using System.Collections.Generic;
using CSharpAdvanced.Core;

namespace CSharpAdvanced
{
    class Program
    {
        static void Main(string[] args)
        {
            var dog = new Dog();
            var cat = new Cat();

            Animal animal = dog;

            animal = cat;


            var reader = new FileReader();
            Console.WriteLine(reader.Read());

            var xmlreader = new XmlReader();
            Console.WriteLine(reader.Read());

            var reader2 = new SecureReader(reader);
            Console.WriteLine(reader2.Read());

            var reader22 = new SecureReader(xmlreader);
            Console.WriteLine(reader2.Read());

            var converter = new GenericConverter();
            var value = converter.Convert<int>("666");
            var value2 = converter.Convert<float>("666");

            var arrlist = new ArrayList();

            //var list = new List<string>();
            //list.Add(123);


            int? abc = null;

            Nullable<int> n;

            M m = new M();
            m?.Test();


        }


        class M
        {
            public void Test()
            {
                
            }

        }
    }


    public class FileSystem
    {
        private IReader _reader;



        //public FileSystem()
        //    : this(new SecureReader(new FileReader()))
        //{
        //}


        public FileSystem(IReader reader)
        {
            _reader = reader;
        }
    }


    public class GenericConverter
    {
        public T Convert<T>(string stringValue)
        {
            var conversionType = typeof (T);
            var changeType = (T) System.Convert.ChangeType(stringValue, conversionType);
            return changeType;
        }



        public object Convert(string stringValue, Type conversionType)
        {
            var changeType = System.Convert.ChangeType(stringValue, conversionType);
            return changeType;
        }
    }
}