﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using TuiTraining.Domain.Iterators;
using TuiTraining.Domain.Model;
using TuiTraining.Domain.Unit.Test.Services;

// ReSharper disable once CheckNamespace
namespace TuiTraining.Domain.Unit.Test.Iterators.UntillAllFlyingObjectsCrashedIteratorTest
{
    [TestFixture]
    public class Given_Valid_Dependencies_When_Constructing_Instance
        : ArrangeActAssertTest
    {
        private UntillAllFlyingObjectsCrashedIterator _sut;



        protected override void Arrange()
        {
        }


        protected override void Act()
        {
            _sut = new UntillAllFlyingObjectsCrashedIterator();
        }


        [Test]
        public void Then_It_Should_Create_A_Valid_Instance()
        {
            Assert.IsNotNull(_sut);
        }
    }



    public abstract class Given_Valid_Dependencies_And_Constructed_Instance_Scenario
        : ArrangeActAssertTest
    {
        protected UntillAllFlyingObjectsCrashedIterator _sut;



        protected override void Arrange()
        {
            _sut = new UntillAllFlyingObjectsCrashedIterator();
        }
    }



    [TestFixture]
    public class Given_All_Flying_Objects_Have_Not_Crashed_When_Checking_Predicate
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private bool _result;
        private IEnumerable<FlyingObject> _flyingObjects;



        protected override void Arrange()
        {
            base.Arrange();

            _flyingObjects =
                new List<FlyingObject>
                {
                    new Airplane("airplane1", "code", null),
                    new Airplane("airplane2", "code", null),
                    new Airplane("airplane3", "code", null)
                };
        }


        protected override void Act()
        {
            _result = _sut.PredicateIsValid(_flyingObjects);
        }


        [Test]
        public void Then_It_Should_Have_Valid_Result_For_The_Predicate()
        {
            Assert.IsTrue(_result);
        }
    }



    [TestFixture]
    public class Given_Some_Flying_Objects_Have_Crashed_When_Checking_Predicate
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private bool _result;
        private IList<FlyingObject> _flyingObjects;



        protected override void Arrange()
        {
            base.Arrange();

            _flyingObjects =
                new List<FlyingObject>
                {
                    new Airplane("airplane1", "code", null),
                    new Airplane("airplane2", "code", null),
                    new Airplane("airplane3", "code", null)
                };

            for (var i = 0; i < _flyingObjects.Count - 1; i++)
            {
                try
                {
                    _flyingObjects[i].Crash();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }


        protected override void Act()
        {
            _result = _sut.PredicateIsValid(_flyingObjects);
        }


        [Test]
        public void Then_It_Should_Have_Valid_Result_For_The_Predicate()
        {
            Assert.IsTrue(_result);
        }
    }



    [TestFixture]
    public class Given_All_Flying_Objects_Have_Crashed_When_Checking_Predicate
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private bool _result;
        private IList<FlyingObject> _flyingObjects;



        protected override void Arrange()
        {
            base.Arrange();

            _flyingObjects =
                new List<FlyingObject>
                {
                    new Airplane("airplane1", "code", null),
                    new Airplane("airplane2", "code", null),
                    new Airplane("airplane3", "code", null)
                };

            foreach (var t in _flyingObjects)
            {
                try
                {
                    t.Crash();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }


        protected override void Act()
        {
            _result = _sut.PredicateIsValid(_flyingObjects);
        }


        [Test]
        public void Then_It_Should_Have_Invalid_Result_For_The_Predicate()
        {
            Assert.IsFalse(_result);
        }
    }
}