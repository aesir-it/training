﻿using NUnit.Framework;

namespace TuiTraining.Domain.Unit.Test.Services
{
    [TestFixture]
    public abstract class ArrangeActAssertTest
    {
        [SetUp]
        public void Setup()
        {
            Arrange();
            Act();
        }


        protected abstract void Arrange();


        protected abstract void Act();
    }
}