﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using TuiTraining.Domain.Iterators.Contracts;
using TuiTraining.Domain.Model;
using TuiTraining.Domain.Procedures.Contracts;
using TuiTraining.Domain.Services;
using TuiTraining.Domain.Services.Contracts;

namespace TuiTraining.Domain.Unit.Test.Services.FlightEngineServiceTest
{
    [TestFixture]
    public class Given_Valid_Dependencies_When_Constructing_Instance
        : ArrangeActAssertTest
    {
        private FlightEngineService _sut;
        private List<FlyingObject> _flyingObjects;
        private IFlightPlanCalculatorService _flightPlanCalculatorService;
        private IProcedureFactory _procedureFactory;
        private IFlyingObjectPredicateIterator _flyingObjectPredicateIterator;



        protected override void Arrange()
        {
            _flyingObjects = new List<FlyingObject>();
            _flightPlanCalculatorService = Mock.Of<IFlightPlanCalculatorService>();
            _procedureFactory = Mock.Of<IProcedureFactory>();
            _flyingObjectPredicateIterator = Mock.Of<IFlyingObjectPredicateIterator>();
        }


        protected override void Act()
        {
            _sut = new FlightEngineService(_flyingObjects, _flightPlanCalculatorService, _procedureFactory, _flyingObjectPredicateIterator);
        }


        [Test]
        public void Then_It_Should_Create_A_Valid_Instance()
        {
            Assert.IsNotNull(_sut);
        }
    }


    [TestFixture]
    public abstract class Given_Valid_Dependencies_And_Constructed_Instance_Scenario
        : ArrangeActAssertTest
    {
        protected FlightEngineService _sut;
        protected Mock<IFlightPlanCalculatorService> _flightPlanCalculatorServiceMock;
        private Mock<IProcedureFactory> _procedureFactoryMock;
        protected Mock<IFlyingObjectPredicateIterator> _flyingObjectPredicateIteratorMock;


        protected abstract List<FlyingObject> FlyingObjects { get; }


        protected override void Arrange()
        {
            _flightPlanCalculatorServiceMock = new Mock<IFlightPlanCalculatorService>();
            var flightPlanCalculatorService = _flightPlanCalculatorServiceMock.Object;

            _procedureFactoryMock = new Mock<IProcedureFactory>();
            var procedureFactory = _procedureFactoryMock.Object;

            _flyingObjectPredicateIteratorMock = new Mock<IFlyingObjectPredicateIterator>();
            var flyingObjectPredicateIterator = _flyingObjectPredicateIteratorMock.Object;

            _sut = new FlightEngineService(FlyingObjects, flightPlanCalculatorService, procedureFactory, flyingObjectPredicateIterator);
        }
    }



    [TestFixture]
    public abstract class Given_A_Valid_FlightPlan_Can_Be_Calculated_When_Running_Scenario
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private int _loopCount;
        protected List<Mock<FlyingObject>> _flyingObjectMocks;
        private List<FlyingObject> _flyingObjects;



        protected override List<FlyingObject> FlyingObjects => _flyingObjects;



        private FlyingObject GetFlyingObject(int idx)
        {
            var flyingObjectMock = new Mock<FlyingObject>($"FlyingObject{idx}", $"Code{idx}", null);
            var flyingObject = flyingObjectMock.Object;

            _flyingObjectMocks.Add(flyingObjectMock);

            return flyingObject;
        }


        protected override void Arrange()
        {
            _loopCount = 0;
            _flyingObjectMocks = new List<Mock<FlyingObject>>();
            _flyingObjects =
                Enumerable
                    .Range(1, 3)
                    .Select(GetFlyingObject)
                    .ToList();

            base.Arrange();

            _flyingObjectPredicateIteratorMock
                .Setup(x => x.PredicateIsValid(_flyingObjects))
                .Returns(() =>
                {
                    _loopCount++;

                    return _loopCount != 3;
                });

            _flightPlanCalculatorServiceMock
                .Setup(x => x.CalculateFlightPlan(It.IsAny<FlyingObject>()))
                .Returns(new FlightPlan(null, null, 666));
        }


        protected override void Act()
        {
            _sut.Run();
        }


        [Test]
        public void Then_It_Should_Iterate_Using_The_FlyingObjectPredicate_Iterator()
        {
            _flyingObjectPredicateIteratorMock.Verify(x => x.PredicateIsValid(_flyingObjects), Times.Exactly(3));
        }


        [Test]
        public void Then_It_Should_Calculate_FlightPlan_For_All_FlyingObjects()
        {
            _flyingObjects.ForEach(fo => _flightPlanCalculatorServiceMock.Verify(x => x.CalculateFlightPlan(fo), Times.Exactly(2)));
        }


        [Test]
        public void Then_It_Should_Make_All_Flying_Objects_Fly()
        {
            _flyingObjectMocks.ForEach(mock => mock.Verify(x => x.Fly(It.IsAny<FlightPlan>(), It.IsAny<IFlyingObjectVisitor>(), It.IsAny<IFlyingObjectVisitor>()), Times.Exactly(2)));
        }
    }



    [TestFixture]
    public class Given_Fly_Does_Not_Throw
        : Given_A_Valid_FlightPlan_Can_Be_Calculated_When_Running_Scenario
    {
        [Test]
        public void Then_it()
        {

        }
    }



    [TestFixture]
    public class Given_Fly_Does_Throw
        : Given_A_Valid_FlightPlan_Can_Be_Calculated_When_Running_Scenario
    {
        private Exception _exception;


        protected override void Arrange()
        {
            base.Arrange();

            Action<Mock<FlyingObject>> exceptionFlyAction = mock =>
                mock
                    .Setup(x => x.Fly(It.IsAny<FlightPlan>(), It.IsAny<IFlyingObjectVisitor>(), It.IsAny<IFlyingObjectVisitor>()))
                    .Throws<Exception>();
            _flyingObjectMocks.ForEach(exceptionFlyAction);
        }


        protected override void Act()
        {
            try
            {
                base.Act();
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }


        [Test]
        public void Then_It_Should_Not_Throw_An_Exception()
        {
            Assert.IsNull(_exception);
        }
    }
}