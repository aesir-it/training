﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using TuiTraining.Domain.Model;
using TuiTraining.Domain.Repositories.Contracts;
using TuiTraining.Domain.Services;

// ReSharper disable once CheckNamespace
namespace TuiTraining.Domain.Unit.Test.Services.FlightPlanCalculatorServiceTest
{
    [TestFixture]
    public class Given_Valid_Dependencies_When_Constructing_Instance
    : ArrangeActAssertTest
    {
        private FlightPlanCalculatorService _sut;
        private IFlightPlanRepository _flightPlanRepository;



        protected override void Arrange()
        {
            _flightPlanRepository = Mock.Of<IFlightPlanRepository>();
        }


        protected override void Act()
        {
            _sut = new FlightPlanCalculatorService(_flightPlanRepository);
        }


        [Test]
        public void Then_It_Should_Create_A_Valid_Instance()
        {
            Assert.IsNotNull(_sut);
        }
    }



    public abstract class Given_Valid_Dependencies_And_Constructed_Instance_Scenario
        : ArrangeActAssertTest
    {
        protected FlightPlanCalculatorService _sut;
        protected Mock<IFlightPlanRepository> _flightPlanRepositoryMock;



        protected override void Arrange()
        {
            _flightPlanRepositoryMock = new Mock<IFlightPlanRepository>();
            var flightPlanRepository = _flightPlanRepositoryMock.Object;

            _sut = new FlightPlanCalculatorService(flightPlanRepository);
        }
    }



    [TestFixture]
    public class Given_No_Suitable_FlightPlan_Found_When_Calculating_Flight_Plan
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private FlightPlan _result;
        private FlyingObject _flyingObject;
        private List<FlightPlan> _flightPlans;
        private Airport _startAirport;
        private Airport _stopAirport;



        protected override void Arrange()
        {
            base.Arrange();

            _startAirport = new Airport(1, "Brussels");
            _stopAirport = new Airport(2, "Malaga");

            _flyingObject = new Airplane("airplane", "code", _stopAirport);

            _flightPlans =
                Enumerable
                    .Range(0, 3)
                    .Select(idx => new FlightPlan(_startAirport, _stopAirport, idx))
                    .ToList();

            _flightPlanRepositoryMock
                .Setup(x => x.All())
                .Returns(_flightPlans);
        }


        protected override void Act()
        {
            _result = _sut.CalculateFlightPlan(_flyingObject);
        }


        [Test]
        public void Then_It_Should_Return_A_Valid_FlightPlan_Instance()
        {
            Assert.IsNotNull(_result);
        }


        [Test]
        public void Then_It_Should_Return_Null_FlightPlan()
        {
            Assert.AreEqual(FlightPlan.Null, _result);
        }


        [Test]
        public void Then_It_Should_Get_All_FlightPlans_From_Repository()
        {
            _flightPlanRepositoryMock.Verify(x => x.All(), Times.Once);
        }
    }



    [TestFixture]
    public class Given_A_Suitable_FlightPlan_Found_When_Calculating_Flight_Plan
        : Given_Valid_Dependencies_And_Constructed_Instance_Scenario
    {
        private FlightPlan _result;
        private FlyingObject _flyingObject;
        private List<FlightPlan> _flightPlans;
        private Airport _startAirport;
        private Airport _stopAirport;



        protected override void Arrange()
        {
            base.Arrange();

            _startAirport = new Airport(1, "Brussels");
            _stopAirport = new Airport(2, "Malaga");

            _flyingObject = new Airplane("airplane", "code", _startAirport);

            _flightPlans =
                Enumerable
                    .Range(0, 3)
                    .Select(idx => new FlightPlan(_startAirport, _stopAirport, idx))
                    .ToList();

            _flightPlanRepositoryMock
                .Setup(x => x.All())
                .Returns(_flightPlans);
        }


        protected override void Act()
        {
            _result = _sut.CalculateFlightPlan(_flyingObject);
        }


        [Test]
        public void Then_It_Should_Return_A_Valid_FlightPlan_Instance()
        {
            Assert.IsNotNull(_result);
        }


        [Test]
        public void Then_It_Should_Not_Return_Null_FlightPlan()
        {
            Assert.AreNotEqual(FlightPlan.Null, _result);
        }
    }
}