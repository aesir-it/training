﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE usp_read_flyingobjects_with_airport
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select
		FO.*,
		A.Name
	from
		FlyingObject FO
		join Airport A
			on FO.CurrentAirportId = A.AirportId;
END
