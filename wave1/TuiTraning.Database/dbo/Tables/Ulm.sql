﻿CREATE TABLE [dbo].[Ulm] (
    [UlmId] INT IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Ulm] PRIMARY KEY CLUSTERED ([UlmId] ASC),
    CONSTRAINT [FK_Ulm_FlyingObject] FOREIGN KEY ([UlmId]) REFERENCES [dbo].[FlyingObject] ([FlyingObjectId])
);

