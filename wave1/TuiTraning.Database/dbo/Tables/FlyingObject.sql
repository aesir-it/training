﻿CREATE TABLE [dbo].[FlyingObject] (
    [FlyingObjectId]   INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (250) NOT NULL,
    [CurrentAirportId] INT            NOT NULL,
    CONSTRAINT [PK_FlyingObject] PRIMARY KEY CLUSTERED ([FlyingObjectId] ASC),
    CONSTRAINT [FK_FlyingObject_Airport] FOREIGN KEY ([CurrentAirportId]) REFERENCES [dbo].[Airport] ([AirportId]) ON DELETE CASCADE ON UPDATE CASCADE
);

