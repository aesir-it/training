﻿CREATE TABLE [dbo].[Helicopter] (
    [HelicopterId] INT NOT NULL,
    CONSTRAINT [PK_Helicopter] PRIMARY KEY CLUSTERED ([HelicopterId] ASC),
    CONSTRAINT [FK_Helicopter_FlyingObject] FOREIGN KEY ([HelicopterId]) REFERENCES [dbo].[FlyingObject] ([FlyingObjectId])
);

